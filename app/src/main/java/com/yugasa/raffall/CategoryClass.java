package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 26/10/17.
 */

public class CategoryClass {
    public String productName;
    public String productDescription;
    public String tokenPrice;
    public String totalToken;
    public String rafflProvider;
    public String aboutRafflProvider;
    public String rafflProviderWebLink;
    public String providerImage;
    public String address;
    public String raffalEndCondition;
    public String deliveryOption;
    public String raffalProductId;
    public String raffalProductImage;
    public String soldToken;
    public String availableToken;
    public String categoryId;

}
