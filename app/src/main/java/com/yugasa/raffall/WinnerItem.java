package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 26/9/17.
 */

public class WinnerItem  {

    public String userReview;
    public String userRating;
    public String productImage;
    public String address;
    public String providerName;
    public String providerImage;
    public String username;
    public String user_image;
    public String winningDate;

}
