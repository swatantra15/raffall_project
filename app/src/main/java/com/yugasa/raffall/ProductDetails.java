package com.yugasa.raffall;

import android.os.Parcel;
import android.os.Parcelable;

import com.yugasa.raffall.Utils.Items.ProductImage;
import com.yugasa.raffall.Utils.Items.ProductInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yugasalabs-45 on 20/9/17.
 */

public class ProductDetails implements Parcelable {
    public String productName;
    public String productDescription;
    public String tokenPrice;
    public String totalToken;
    public String raffleProvider;
    public String aboutRaffleProvider;
    public String raffleProviderWeblink;
    public String address;
    public String raffalproductId;
    public List<ProductImage> productImages;
    public String providerImage;
    public String raffalEndCondition;
    public String status;
    public String modifiedOn;
    public String countRaffleHistory;
    public int average_raffle_history_rating;
    public String createdon;
    public String deliveryOption;
    public String soldToken;
    public String freeToken;
    public ProductDetails(){}

    protected ProductDetails(Parcel in) {
        productName = in.readString();
        productDescription = in.readString();
        tokenPrice = in.readString();
        totalToken = in.readString();
        raffleProvider = in.readString();
        aboutRaffleProvider = in.readString();
        raffleProviderWeblink = in.readString();
        address = in.readString();
        raffalproductId = in.readString();
        productImages = in.createTypedArrayList(ProductImage.CREATOR);
        providerImage = in.readString();
        raffalEndCondition = in.readString();
        average_raffle_history_rating=in.readInt();
        status = in.readString();
        modifiedOn = in.readString();
        createdon = in.readString();
        deliveryOption = in.readString();
        soldToken = in.readString();
        freeToken = in.readString();
        if (in.readByte()==0x01){
            productImages=new ArrayList<ProductImage>();
            in.readList(productImages,ProductInfo.class.getClassLoader());
        }
        else{
            productImages=null;
        }
    }

    public static final Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {
        @Override
        public ProductDetails createFromParcel(Parcel in) {
            return new ProductDetails(in);
        }

        @Override
        public ProductDetails[] newArray(int size) {
            return new ProductDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productName);
        parcel.writeString(productDescription);
        parcel.writeString(tokenPrice);
        parcel.writeString(totalToken);
        parcel.writeString(raffleProvider);
        parcel.writeString(aboutRaffleProvider);
        parcel.writeString(raffleProviderWeblink);
        parcel.writeString(address);
        parcel.writeString(raffalproductId);
        parcel.writeTypedList(productImages);
        parcel.writeString(providerImage);
        parcel.writeString(raffalEndCondition);
        parcel.writeString(status);
        parcel.writeString(modifiedOn);
        parcel.writeString(createdon);
        parcel.writeString(deliveryOption);
        parcel.writeString(soldToken);
        parcel.writeString(freeToken);
        if (productImages == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(productImages);
        }
    }
}


