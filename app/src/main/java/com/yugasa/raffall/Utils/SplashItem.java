package com.yugasa.raffall.Utils;

/**
 * Created by yugasalabs-45 on 25/8/17.
 */

public class SplashItem  {

    String title;
    int drawable;



    public SplashItem(String title, int drawable) {
        this.title = title;
        this.drawable = drawable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }


}
