package com.yugasa.raffall.Utils.Items;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.yugasa.raffall.Adapters.CustomDialogAdapter;
import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.CustomDialogEntity;
import com.yugasa.raffall.Utils.ReferEntity;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by yugasalabs-16 on 7/11/17.
 */

public class Constant {
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    String urlFb="https://www.facebook.com/RafflTicketApp/";
    private ArrayList<ReferEntity> list;
    private ArrayList<CustomDialogEntity> customDialogEntities;
    public static   String SHARE_TEXT=" I just entered a Raffl to win this amazing prize! Raffl Ticket app has hundreds of prizes to giveaway with incredible low prices and high chances of winning. \n" +
            "           This is my referral code please enter the following referral code";
    public  void ShowDialog(final Context context, final String text, Fragment fragment, AppCompatActivity appCompatActivity, final String image){
        FacebookSdk.sdkInitialize(context);
        callbackManager = CallbackManager.Factory.create();
        if(fragment!=null) {
            shareDialog = new ShareDialog(fragment);
        }
        else if(appCompatActivity!=null){
            shareDialog=new ShareDialog(appCompatActivity);
        }
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }

        });
        final Dialog dialog=new Dialog(context);
        View layout= LayoutInflater.from(context).inflate(R.layout.layout_sharedialog,null);
        dialog.setTitle("Share Via");
        dialog.setContentView(layout);
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        LinearLayoutManager layoutManager=new LinearLayoutManager(context);
        RecyclerView recyclerView= (RecyclerView) layout.findViewById(R.id.referRecyclerView);
        recyclerView.setLayoutManager(layoutManager);

        share.setType("text/plain");
        // gets the list of intents that can be loaded.
        final List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, 0);
        list=new ArrayList<>();
        customDialogEntities=new ArrayList<>();
        int fbIndx=-1;
        for(int i=0; i<resInfo.size();i++){
            Drawable d=resInfo.get(i).loadIcon(context.getPackageManager());
            final CustomDialogEntity customDialogEntity=new CustomDialogEntity();

            customDialogEntity.name=resInfo.get(i).activityInfo.loadLabel(context.getPackageManager()).toString();
            customDialogEntity.image=d;
            customDialogEntity.packagename=resInfo.get(i).activityInfo.packageName;

            customDialogEntities.add(customDialogEntity);

//             ReferEntity referEntity=new ReferEntity(resInfo.get(i).activityInfo.loadLabel(context.getPackageManager()).toString(),resInfo.get(i).activityInfo.loadIcon(context.getPackageManager()));
//
//             list.add(referEntity);

boolean is_facebook = false;
for(int j=0;j<customDialogEntities.size();j++){
    CustomDialogEntity customDialogEntity1=customDialogEntities.get(j);
    if(!customDialogEntity1.name.equalsIgnoreCase("Facebook")){
        is_facebook=true;
        break;
    }
}
if(is_facebook){
CustomDialogEntity custom=new CustomDialogEntity();
custom.name="Facebook";
custom.packagename="com.facebook";
custom.image=context.getDrawable(R.drawable.facebbok);
customDialogEntities.add(custom);

}

        recyclerView.setAdapter(new CustomDialogAdapter(customDialogEntities, new CustomDialogAdapter.Share() {
            @Override
            public void OnShare(View view, final int position) {
                if (customDialogEntities.get(position).name.equalsIgnoreCase("Facebook")) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent;



                            linkContent = new ShareLinkContent.Builder()

                                    .setContentTitle(SHARE_TEXT+": "+text)
                                    .setContentUrl(Uri.parse("google.com"))
                                    .setContentDescription(SHARE_TEXT+": "+text)
                                    .build();



                        shareDialog.show(linkContent);
                    }
                    dialog.dismiss();
                }


                else{
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setPackage(customDialogEntities.get(position).packagename);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, SHARE_TEXT+": "+text );
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                    dialog.dismiss();
                }
            }
        }));

        dialog.show();

    }}
    private void SaveImage(Bitmap finalBitmap , String text) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Vsay_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String name= text.substring(0, Math.min(text.length(), 6));
        String fname = name +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
//            image_name=file;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
