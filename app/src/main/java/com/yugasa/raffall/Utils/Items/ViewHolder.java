package com.yugasa.raffall.Utils.Items;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yugasa.raffall.R;

/**
 * Created by yugasalabs-45 on 1/9/17.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    TextView description;
    ImageView imageView;

    public ViewHolder(View itemView) {
        super(itemView);
        title= itemView.findViewById(R.id.textViewTitle);
        description=itemView.findViewById(R.id.textViewDescription);
        imageView= itemView.findViewById(R.id.imageviewItem);

    }


}

