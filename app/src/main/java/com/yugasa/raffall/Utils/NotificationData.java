package com.yugasa.raffall.Utils;

import java.io.Serializable;

/**
 * Created by yugasalabs-45 on 31/10/17.
 */

public class NotificationData implements Serializable{
    public String winnerId;
    public String userName;
    public String userImage;
    public String productName;
    public String productImage;
    public String winningDate;
    public String type;


}
