package com.yugasa.raffall.Utils;

/**
 * Created by nzt on 2/12/15.
 */
public class ReferEntity
{
    private String text;
    private int resId;

    public ReferEntity(String text, int resId) {
        this.text = text;
        this.resId = resId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
