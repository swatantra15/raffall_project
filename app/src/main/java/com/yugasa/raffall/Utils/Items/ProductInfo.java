package com.yugasa.raffall.Utils.Items;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yugasalabs-45 on 13/9/17.
 */

public class ProductInfo implements Parcelable {

    public String id;
    public String user_id;
    public String raffalProductId;
    public String NoOfTokens;
    public String transactionId;
    public String status;
    public String createdOn;
    public String productImage;

    public ProductInfo(){}
    protected ProductInfo(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        raffalProductId = in.readString();
        NoOfTokens = in.readString();
        transactionId = in.readString();
        status = in.readString();
        createdOn = in.readString();
        productImage = in.readString();
    }

    public static final Creator<ProductInfo> CREATOR = new Creator<ProductInfo>() {
        @Override
        public ProductInfo createFromParcel(Parcel in) {
            return new ProductInfo(in);
        }

        @Override
        public ProductInfo[] newArray(int size) {
            return new ProductInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(user_id);
        parcel.writeString(raffalProductId);
        parcel.writeString(NoOfTokens);
        parcel.writeString(transactionId);
        parcel.writeString(status);
        parcel.writeString(createdOn);
        parcel.writeString(productImage);
    }
}