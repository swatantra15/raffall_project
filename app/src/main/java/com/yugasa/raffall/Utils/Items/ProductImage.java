package com.yugasa.raffall.Utils.Items;

import android.os.Parcel;
import android.os.Parcelable;

import com.yugasa.raffall.Utils.ProductItems;

/**
 * Created by yugasalabs-45 on 13/9/17.
 */

public class ProductImage implements Parcelable{

    public String id;
    public String raffallProductId;
    public String image;
    public String createdOn;
    public String modifiedOn;

    public ProductImage(){}

    protected ProductImage(Parcel in) {
        id = in.readString();
        raffallProductId = in.readString();
        image = in.readString();
        createdOn = in.readString();
        modifiedOn = in.readString();
    }

    public static final Creator<ProductImage> CREATOR = new Creator<ProductImage>() {
        @Override
        public ProductImage createFromParcel(Parcel in) {
            return new ProductImage(in);
        }

        @Override
        public ProductImage[] newArray(int size) {
            return new ProductImage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(raffallProductId);
        parcel.writeString(image);
        parcel.writeString(createdOn);
        parcel.writeString(modifiedOn);
    }
}
