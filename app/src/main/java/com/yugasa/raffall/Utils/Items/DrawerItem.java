package com.yugasa.raffall.Utils.Items;

/**
 * Created by yugasalabs-45 on 1/9/17.
 */

public class DrawerItem {
   public String title;
   public String description;
  public int imageId;


    public DrawerItem(String title, String description, int imageId) {
        this.imageId = imageId;
        this.title=title;
        this.description=description;

    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
