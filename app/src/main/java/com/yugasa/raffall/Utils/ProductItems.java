package com.yugasa.raffall.Utils;

import android.os.Parcel;
import android.os.Parcelable;

import com.yugasa.raffall.Utils.Items.ProductImage;
import com.yugasa.raffall.Utils.Items.ProductInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yugasalabs-45 on 13/9/17.
 */

public class ProductItems implements Parcelable,Serializable {
    public String productName;
    public String productDescription;
    public String tokenPrice;
    public String totalToken;
    public String raffleProvider;
    public String aboutRaffleProvider;
    public String raffleProviderWeblink;
    public String address;
    public String productId;
    public String productImage;

    public String soldToken;
    public String freeToken;
    public String productWinningPrice;
    public String raffleEndCondition;
    public String deliveryOption;
    public String status;
    public String createdOn;
    public String modifiedOn;
    public String providerImage;
    public List<ProductInfo> productInfoList;
    public List<ProductImage> productImages;

    public ProductItems(){}

    protected ProductItems(Parcel in) {
        productName = in.readString();
        productDescription = in.readString();
        tokenPrice = in.readString();
        totalToken = in.readString();
        raffleProvider = in.readString();
        aboutRaffleProvider = in.readString();
        raffleProviderWeblink = in.readString();
        address = in.readString();
        productId = in.readString();
        productImage = in.readString();
        soldToken = in.readString();
        freeToken = in.readString();
        productWinningPrice = in.readString();
        raffleEndCondition = in.readString();
        deliveryOption = in.readString();
        status = in.readString();
        createdOn = in.readString();
        modifiedOn = in.readString();
        providerImage = in.readString();
        if (in.readByte()==0x01){
            productInfoList=new ArrayList<ProductInfo>();
            in.readList(productInfoList,ProductInfo.class.getClassLoader());
        }
        else{
            productInfoList=null;
        }
        if (in.readByte()==0x01){
            productImages=new ArrayList<ProductImage>();
            in.readList(productImages,ProductImage.class.getClassLoader());
        }
        else{
            productImages=null;
        }
    }

    public static final Creator<ProductItems> CREATOR = new Creator<ProductItems>() {
        @Override
        public ProductItems createFromParcel(Parcel in) {
            return new ProductItems(in);
        }

        @Override
        public ProductItems[] newArray(int size) {
            return new ProductItems[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productName);
        parcel.writeString(productDescription);
        parcel.writeString(tokenPrice);
        parcel.writeString(totalToken);
        parcel.writeString(raffleProvider);
        parcel.writeString(aboutRaffleProvider);
        parcel.writeString(raffleProviderWeblink);
        parcel.writeString(address);
        parcel.writeString(productId);
        parcel.writeString(productImage);
        parcel.writeString(soldToken);
        parcel.writeString(freeToken);
        parcel.writeString(productWinningPrice);
        parcel.writeString(raffleEndCondition);
        parcel.writeString(deliveryOption);
        parcel.writeString(status);
        parcel.writeString(createdOn);
        parcel.writeString(modifiedOn);
        parcel.writeString(providerImage);

        if (productInfoList == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(productInfoList);
        }
        if (productImages == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(productImages);
        }
    }
}