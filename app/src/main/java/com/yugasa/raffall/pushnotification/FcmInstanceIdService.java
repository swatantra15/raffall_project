package com.yugasa.raffall.pushnotification;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.RecentToken;


/**
 * Created by yugasalabs_18 on 11/11/16.
 */
public class FcmInstanceIdService extends FirebaseInstanceIdService {
    public static String recent_token;
    @Override
    public void onTokenRefresh() {
        recent_token= FirebaseInstanceId.getInstance().getToken();
        RecentToken recentToken=new RecentToken();
        recentToken.token=recent_token;
        Log.i("token",recent_token);
        DroidPrefs.apply(this,"token",recentToken);


    }
}
