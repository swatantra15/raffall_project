package com.yugasa.raffall.pushnotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.yugasa.raffall.Activity.Card_ItemActivity;
import com.yugasa.raffall.Activity.Looser_Notification;
import com.yugasa.raffall.Activity.Social_Login_Link;
import com.yugasa.raffall.Activity.Winner_notification;
import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.NotificationData;
import com.yugasa.raffall.volley.MySingleton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

//import gray.patient.R;

//import gray.patient.MyMedicineListActivity;
//import gray.patient.R;
//import gray.patient.custom.DroidPrefs;
//import gray.patient.custom.NotificationCount;
//import gray.patient.volley.MySingleton;

/**
 * Created by yugasalabs_18 on 11/11/16.
 */
public class FcmMessagingService extends FirebaseMessagingService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int FLAG_ONE_SHOT = 0;
    ImageLoader imageLoader;

    Bitmap bitmap;
    PendingIntent pendingIntent=null;
    Intent intent=null;
    private GoogleApiClient mGoogleApiClient;

    Double lat;
    Double langt;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = new HashMap<>();
        data = remoteMessage.getData();

        Random r = new Random();
        int random = r.nextInt(10000);
        imageLoader = MySingleton.getInstance(this).getImageLoader();

        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(this);
//        String message= remoteMessage.getNotification().getBody();

//        Intent intent = new Intent(this, MyMedicineListActivity.class);

//        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 12.9667, 77.5667);
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//        PendingIntent mapPendingIntent =
//                PendingIntent.getActivity(this, 0, mapIntent, 0);
//        NotificationCompat.Action wearableAction = new NotificationCompat.Action.Builder(
//                android.R.drawable.ic_dialog_map,"open activity",mapPendingIntent).build();

        //class to specify wearable action
//        NotificationCompat.WearableExtender wearableExtender =
//                new NotificationCompat.WearableExtender().addAction(wearableAction)
//                        .setHintHideIcon(true);
        String type = data.get("type");
        if(type.equalsIgnoreCase("raffle_winner")){
            NotificationData notificationData=new NotificationData();
            notificationData.winnerId=data.get("winner_id");
            notificationData.userName=data.get("user_name");
            notificationData.userImage=data.get("user_image");
            notificationData.productName=data.get("raffle_product_name");
            notificationData.productImage=data.get("raffle_product_image");
            notificationData.winningDate=data.get("winning_date");

            Intent intent = new Intent(this, Winner_notification.class);
            intent.putExtra("data",notificationData);
            intent.putExtra("type",1);

            notificationCompat.setContentTitle(data.get("title"));
            notificationCompat.setContentText(data.get("tAlert_body"));
            pendingIntent = PendingIntent.getActivity(this, random, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        }
        else if(type.equalsIgnoreCase("raffle_looser")){
            NotificationData notificationData=new NotificationData();
            notificationData.userName=data.get("user_name");
            notificationData.userImage=data.get("user_image");
            notificationData.productName=data.get("raffle_product_name");
            Intent intent = new Intent(this,Looser_Notification.class);
            //intent.putExtra("productID",data.get("raffle_product_id"));
           // intent.putExtra("type",1);
            intent.putExtra("data",notificationData);
            notificationCompat.setContentTitle(data.get("title"));
            notificationCompat.setContentText(data.get("tAlert_body"));
            pendingIntent = PendingIntent.getActivity(this, random, intent, PendingIntent.FLAG_CANCEL_CURRENT);


        }
        else if(type.equalsIgnoreCase("nearly_ending")){
            Intent intent = new Intent(this, Card_ItemActivity.class);
            intent.putExtra("productID",data.get("raffle_product_id"));
            intent.putExtra("type",1);
            notificationCompat.setContentTitle(data.get("title"));
            notificationCompat.setContentText(data.get("tAlert_body"));
            pendingIntent = PendingIntent.getActivity(this, random, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        }
        else if(type.equalsIgnoreCase("add_new_raffl")){
            Intent intent = new Intent(this, Card_ItemActivity.class);
            intent.putExtra("productID",data.get("raffle_product_id"));
            intent.putExtra("type",1);
            notificationCompat.setContentTitle(data.get("title"));
            notificationCompat.setContentText(data.get("tAlert_body"));
            pendingIntent = PendingIntent.getActivity(this, random, intent, PendingIntent.FLAG_CANCEL_CURRENT);


        }




//        if(daydose.dosageForm.equalsIgnoreCase("Pill")){
//            holder.img_reminder_item.setImageResource(R.mipmap.pill);
//        }
//        else if(daydose.dosageForm.equalsIgnoreCase("Spray")) {
//            holder.img_reminder_item.setImageResource(R.mipmap.spray);
//        }
//        else if (daydose.dosageForm.equalsIgnoreCase("Dropper")){
//            holder.img_reminder_item.setImageResource(R.mipmap.drops);
//        }
//        else if (daydose.dosageForm.equalsIgnoreCase("Powder")){
//            holder.img_reminder_item.setImageResource(R.mipmap.powder);
//        }
//        else if (daydose.dosageForm.equalsIgnoreCase("Others")){
//            holder.img_reminder_item.setVisibility(View.GONE);
//        }
//        holder.img_reminder_item.setColorFilter(Color.parseColor("#42a2ec"));
//
//        holder.txt_dose_time.setText(timehours+":"+timeminutes+" "+am);


//
//        notificationCompat.setContentTitle(data.get("firstName") + " " + data.get("lastName"));

////            Dr Peter Simons reminds you, at 15:30
//            final SpannableStringBuilder sb = new SpannableStringBuilder("Dr "+data.get("firstName") + " " + data.get("lastName") + getString(R.string.remind_you_at)+time_of_day);
//            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
//            sb.setSpan(bss,0,sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//            notificationCompat.setContentTitle(sb);
//            notificationCompat.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.take_your_medicine) + "\n"+ data.get("dosage") + getString(R.string.doses_of) + " " + data.get("medicine")));
////            notificationCompat.setContentText("Take  your medicine " + "\n"+ data.get("dosage") + " doses" + "of" + " " + data.get("medicine"));
//
//        } else if (type != null && type.equalsIgnoreCase("PrescribeMedicine")) {
//
//            notificationCompat.setContentTitle(data.get("firstName") + " " + data.get("lastName"));
//            notificationCompat.setContentText(  getString(R.string.prescribed)  + data.get("medicine") + " " + getString(R.string.for_you));
//        }
//        else if(type!=null && type.equalsIgnoreCase("AddText")){
//            final SpannableStringBuilder sb = new SpannableStringBuilder("Dr "+data.get("firstName") + " " + data.get("lastName"));
//            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
//            sb.setSpan(bss,0,sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//            notificationCompat.setContentTitle(sb);
//            notificationCompat.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.prescribed)+ data.get("text") + " " + getString(R.string.for_you)));
////            notificationCompat.setContentTitle(data.get("firstName") + " " + data.get("lastName"));
////            notificationCompat.setContentText();
//        }
//        else if(type!=null && type.equalsIgnoreCase("addTreatment")){
////            notificationCompat.setContentTitle(data.get("firstName") + " " + data.get("lastName"));
////            notificationCompat.setContentText("Prescribed  " + data.get("medicine") + " " + "for You");
//            final SpannableStringBuilder sb = new SpannableStringBuilder("Dr "+data.get("firstName") + " " + data.get("lastName"));
//            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
//            sb.setSpan(bss,0,sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//            notificationCompat.setContentTitle(sb);
//            notificationCompat.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.prescribed)   + data.get("medicine") + " " + getString(R.string.for_you)));
//        }








        if (Build.VERSION.SDK_INT >= 21) {
            notificationCompat.setSmallIcon(R.mipmap.ic_launcher);
        } else {
            notificationCompat.setSmallIcon(R.mipmap.ic_launcher);
        }
        if (bitmap != null) {
            notificationCompat.setLargeIcon(bitmap);
        } else {
            notificationCompat.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

        }

        notificationCompat.setAutoCancel(true);
        notificationCompat.setContentIntent(pendingIntent);
        notificationCompat.setDefaults(Notification.DEFAULT_ALL);
        notificationCompat.setPriority(Notification.PRIORITY_HIGH);
        notificationCompat.setLocalOnly(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(random, notificationCompat.build());




//        generateWearNotification(data.get("addedBy"), data.get("medicine"), data.get("firstName"), data.get("lastName"), data.get("reminderId"), data.get("medicineId"), time_of_day, type, Constants.WATCH_ONLY_PATH, data.get("dosage"));



    }
//    Ayush Jain (ayushjain.yugasa@gmail.com)
//
//                                'winner_id' =>  $status->id,
//            'user_name' => $user->name,
//            'user_image' => Configure::read('user_profile_imagepath').$user->image,
//            'raffle_product_name'  => $result->product_name,
//            'raffle_product_image' => Configure::read('product_imagepath').$raffle_product_images['image'],
//            'winning_date' => date("jS F Y", strtotime($won_date)),
//            'status'    =>  "1",
//            'type'  =>  "raffle_winner",

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        mLocationRequest = LocationRequest.create();
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setFastestInterval(1000 * 60 * 5); // Update location every second
//        mLocationRequest.setSmallestDisplacement(0);
//
//
//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, this);
//        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
      //  mGoogleApiClient.disconnect();
    }

    private String now() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return sdf.format(new Date());
    }




}
