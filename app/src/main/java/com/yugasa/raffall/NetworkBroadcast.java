package com.yugasa.raffall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by yugasalabs-45 on 24/11/17.
 */

public class NetworkBroadcast extends BroadcastReceiver {
    boolean val=false;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                Toast.makeText(context, "network detectd", Toast.LENGTH_SHORT).show();
                Log.d("Network", "Internet YAY");
                Application.is_netconnected=true;

            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                Log.d("Network", "No internet :(");
                Toast.makeText(context, "no internet", Toast.LENGTH_SHORT).show();

                Application.is_netconnected=false;

               /* if (val==false) {
                    displayAlert(context);
                    val=true;
                }
*/
            }
        }
    }

    private void displayAlert(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getApplicationContext());
        builder.setMessage("No Internet Connection").setCancelable(
                false).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        val=false;
                        int pid = android.os.Process.myPid();
                        android.os.Process.killProcess(pid);
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        context.startActivity(intent);

                        dialog.dismiss();


                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
