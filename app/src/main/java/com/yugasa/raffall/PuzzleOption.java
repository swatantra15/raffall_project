package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 23/10/17.
 */

public class PuzzleOption {
    public String id;
    public String questionId;
    public String optionValue;
    public String optionFormat;
}
