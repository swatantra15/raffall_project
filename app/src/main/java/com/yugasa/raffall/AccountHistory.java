package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 25/10/17.
 */

public class AccountHistory  {
    public String id;
    public String user_id;
    public String paypal_id;
    public String credit_amount;
    public String reason;
    public String status;
    public String create_time;
    public String type;

}
