package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 27/9/17.
 */

public class HistoryDetails {
    public  String userName;
    public  String userAddress;
    public  String userImage;
    public  String userRating;
    public  String userReview;
    public  String productName;
    public  String productTokenPrice;
    public  String soldToken;
    public  String endDate;
    public  String raffalProductImage;


}

