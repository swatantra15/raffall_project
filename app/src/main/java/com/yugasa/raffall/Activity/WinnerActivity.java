package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.Adapters.SoldTicketItem;
import com.yugasa.raffall.Adapters.Winner_Adaper;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.WinnerItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WinnerActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbarTitle,noTextFound;
    ImageView backIcon,shareIcon;
    List<WinnerItem> winnerItems;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
Winner_Adaper adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        recyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        shareIcon=(ImageView)findViewById(R.id.share);
        toolbarTitle=(TextView)findViewById(R.id.toolbarTitle);
        noTextFound=(TextView)findViewById(R.id.no_data_text);

        toolbarTitle.setText(R.string.item_title5);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        winnerItems=new ArrayList<>();


        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        shareIcon.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWinnerDetails();
        adapter=new Winner_Adaper(this,winnerItems);
        recyclerView.setAdapter(adapter);
    }

    public void getWinnerDetails(){
        String url= APIConstantsDemo.WINNER_URL;
        Log.i("url",url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching Winner Info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){

                            JSONArray jsonArray=response.getJSONArray("data");
                            if(winnerItems.size()>0){
                                winnerItems.clear();
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {
                                WinnerItem winnerItem=new WinnerItem();

                                    JSONObject json1 = jsonArray.getJSONObject(i);

                                    winnerItem.username = json1.getString("user_name");
                                    winnerItem.user_image = json1.getString("user_image");
                                    winnerItem.address = json1.getString("user_address");
                                    winnerItem.productImage = json1.getString("raffle_product_image");
                                    winnerItem.providerName = json1.getString("raffle_provider_name");
                                    winnerItem.providerImage = json1.getString("raffle_provider_image");
                                    winnerItem.userReview = json1.getString("review");
                                    winnerItem.userRating = json1.getString("rating");
                                    winnerItem.winningDate = json1.getString("winning_date");



                                winnerItems.add(winnerItem);

                            }
                            if(winnerItems.size()>0){
                                recyclerView.setVisibility(View.VISIBLE);
                                noTextFound.setVisibility(View.GONE);

                            }
                            else{
                                recyclerView.setVisibility(View.GONE);
                                noTextFound.setVisibility(View.VISIBLE);

                            }


                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

}
