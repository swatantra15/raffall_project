package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.yugasa.raffall.Account_Info;
import com.yugasa.raffall.Adapters.Drawer_Adapter;
import com.yugasa.raffall.Adapters.Swipe_Deck_Adapter;
import com.yugasa.raffall.Constant;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.NetworkBroadcast;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.Items.DrawerItem;
import com.yugasa.raffall.Utils.ProductItems;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class MainActivity extends AppCompatActivity {

    public List<DrawerItem> data;
    List<UserInfo> userInfoList;
    List<ProductItems> productItemsList;
    UserInfo userInfo;
    Handler handler;
    @BindView(R.id.txt_title_profile)
    TextView profileText;
    @BindView(R.id.appLogo)
    ImageView applogo;
    Dialog dialog;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    Boolean flag = false;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    // @InjectView(R.id.swipe_deck)
    //SwipeDeck swipeDeck;
    @BindView(R.id.swipe_card)
    CardStackView swipeDeck;
    Intent intent;
    @BindView(R.id.recyclerViewDrawer)
    RecyclerView recyclerView;
    @BindView(R.id.profile_image)
    ImageView profileImage;
    @BindView(R.id.info)
    ImageView infoIcon;
    @BindView(R.id.logoutAccount)
    Button logOut;
    TextView ok, txtHead, buttonYes;
    TextView noConnection, buttonNo;
    @BindView(R.id.walletMoney)
    TextView walletMoney;
    ProgressDialog progressDialog;
    Swipe_Deck_Adapter swipe_deck_adapter;
    int num_times = 0;

    public static int POSITION;

    boolean IS_CHECK_DRAWER=false;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        data = fill_with_data();
        productItemsList = new ArrayList<>();
        Log.d("", "");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);

        //this code paste from onstart here
        handler = new Handler();
        intent = getIntent();
        if (intent != null) {
            if (intent.getParcelableArrayListExtra("product_item") != null) {
                if (productItemsList.size() > 0) {
                    productItemsList.clear();
                }
                   IS_CHECK_DRAWER=true;
                 productItemsList = intent.getParcelableArrayListExtra("product_item");
                swipe_deck_adapter = new Swipe_Deck_Adapter(MainActivity.this);
                swipe_deck_adapter.addAll(productItemsList);
                swipeDeck.setAdapter(swipe_deck_adapter);
            } else {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getUserbyPhonenumber();
                    }
                }, 1000);
            }

        }

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {showdialog();


            }
        });


        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Card_ItemActivity.class);
                int position, index;
                index = swipeDeck.getTopIndex();
                if (num_times == 0) {
                    position = index;
                } else {
                    if (index >= productItemsList.size()) {

                        position = index - productItemsList.size() * num_times;
                        if (position == -1) {
                            position = productItemsList.size() - 1;

                        } else if (position == -2) {
                            position = productItemsList.size() - 2;
                        }
                    } else {
                        position = index;
                    }
                }
//

                intent.putExtra("productID", productItemsList.get(position).productId);
                startActivity(intent);
            }
        });




        walletMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyAccountActivity.class);

                startActivity(intent);
            }
        });


        swipeDeck.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
                Log.d("CardStackView", "onCardDragging");
            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                Log.d("CardStackView", "onCardSwiped: " + direction.toString());
                Log.d("CardStackView", "topIndex: " + swipeDeck.getTopIndex());
                if (swipeDeck.getTopIndex() == swipe_deck_adapter.getCount()-1) {
                    Log.d("CardStackView", "Paginate: " + swipeDeck.getTopIndex());
                    num_times++;
                    paginate();
                }
                else if(swipeDeck.getTopIndex()==swipe_deck_adapter.getCount()){
                    Log.d("CardStackView", "Paginate: " + swipeDeck.getTopIndex());
                    num_times++;
                    paginate();
                }



//                } //else if (swipeDeck.getTopIndex() == swipe_deck_adapter.getCount() - 1) {
//                    Log.d("CardStackView", "Paginate: " + swipeDeck.getTopIndex());
//                    num_times++;
//                    paginate();
//                }
            }

            @Override
            public void onCardReversed() {
                Log.d("CardStackView", "onCardReversed");
                swipeDeck.reverse();
            }

            @Override
            public void onCardMovedToOrigin() {
                Log.d("CardStackView", "onCardMovedToOrigin");
            }

            @Override
            public void onCardClicked(int index) {
                Log.d("CardStackView", "onCardClicked: " + index);

                Intent intent = new Intent(MainActivity.this, Card_ItemActivity.class);
                int position;
                if (num_times == 0) {
                    position = index;
                } else {
                    if (index >= productItemsList.size()) {

                        position = index - productItemsList.size() * num_times;
                        if (position == -1) {
                            position = productItemsList.size() - 1;
                        } else if (position == -2) {
                            position = productItemsList.size() - 2;
                        }
                    } else {
                        position = index;
                    }
                }
//                if(index>=productItemsList.size()){
//                    position=index-productItemsList.size();
//                }
//                else {
//                    position=index;
//                }
                intent.putExtra("productID", productItemsList.get(position).productId);
                startActivity(intent);
            }
        });


        /*swipeDeck.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
            }

            @Override
            public void cardSwipedRight(int position) {
                Log.i("MainActivity", "card was swiped right, position in adapter: " + position);
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");

                swipe_deck_adapter = new Swipe_Deck_Adapter(productItemsList, MainActivity.this);
                swipeDeck.setAdapter(swipe_deck_adapter);

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });*/
        userInfo = DroidPrefs.get(this, "user_info", UserInfo.class);


//        Log.e("list",userInfo.name);

        Glide.with(this).load(userInfo.profile_image).into(profileImage);
        profileText.setText(userInfo.name);

        Glide.with(this).load(userInfo.profile_image)
                .apply(bitmapTransform(new BlurTransformation(10)))
                .into((ImageView) findViewById(R.id.header_background));

        // setSupportActionBar(toolbar);
        final Drawer_Adapter adapter = new Drawer_Adapter(data, this, new Drawer_Adapter.Clicked() {
            @Override
            public void Click(View view, int position) {


                if (position == 0) {
                    Intent intent = new Intent(MainActivity.this, Category_Activity.class);
                    startActivity(intent);

                } else if (position == 1) {
                    Intent intent = new Intent(MainActivity.this, Social_Login_Link.class);
                    intent.putExtra("type",1);
                    startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(MainActivity.this, MyAccountActivity.class);
                    startActivity(intent);
                } else if (position == 3) {
                    Intent intent = new Intent(MainActivity.this, MyRaffllesActivity.class);
                    startActivity(intent);
                } else if (position == 4) {
                    Intent intent = new Intent(MainActivity.this, WinnerActivity.class);
                    startActivity(intent);
                } else if (position == 5) {
                    Intent intent = new Intent(MainActivity.this, GetFreeCreditActivity.class);
                    startActivity(intent);
                } else if (position == 6) {
                    Intent intent = new Intent(MainActivity.this, RulesAndGuidelines.class);
                    startActivity(intent);
                } else if (position == 7) {
                    Intent intent = new Intent(MainActivity.this, MoreActivity.class);
                    startActivity(intent);
                }

            }
        });


        recyclerView.setAdapter(adapter);
    }



    @Override
    protected void onResume() {
        super.onResume();

        getAccountInfo();
       // progressDialog.dismiss();

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                getAccountInfo();
               // progressDialog.dismiss();
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);
        //  toolbar.setNavigationIcon(R.drawable.back);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menubaricon);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IS_CHECK_DRAWER) {
                    drawer.openDrawer(Gravity.LEFT);
                }

            }
        });


        applogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(Gravity.START);

            }
        });

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    //        imageView.setOnTouchListener(new View.OnTouchListener() {
//
//            public boolean onTouch(View v, MotionEvent event) {
//                // TODO Auto-generated method stub
//                drag(event, v);
//                return true;
//            }
//        });
//    }

//    public void drag(MotionEvent event, View v) {
//
//        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) v.getLayoutParams();
//
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_MOVE: {
//                params.topMargin = (int) event.getRawY() - (v.getHeight());
//                params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
//                v.setLayoutParams(params);
//                break;
//            }
//            case MotionEvent.ACTION_UP: {
//                params.topMargin = (int) event.getRawY() - (v.getHeight());
//                params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);
//                v.setLayoutParams(params);
//                break;
//            }
//            case MotionEvent.ACTION_DOWN: {
//                v.setLayoutParams(params);
//                break;
//            }
//        }



    public List<DrawerItem> fill_with_data(){
        List<DrawerItem> data=new ArrayList<>();
        data.add(new DrawerItem(getString(R.string.item_title1),getString(R.string.item_descrip_1),R.drawable.category_icon));
        data.add(new DrawerItem(getString(R.string.item_title2),getString(R.string.item_descrip_2),R.drawable.link_icon));
        data.add(new DrawerItem(getString(R.string.item_title3),getString(R.string.item_descrip_3),R.drawable.my_account));
        data.add(new DrawerItem(getString(R.string.item_title4),getString(R.string.item_descrip_4),R.drawable.tickets));
        data.add(new DrawerItem(getString(R.string.item_title5),getString(R.string.item_descrip_5),R.drawable.winners));
        data.add(new DrawerItem(getString(R.string.item_title6),getString(R.string.item_descrip_6),R.drawable.credits));
        data.add(new DrawerItem(getString(R.string.item_title7),getString(R.string.item_descrip_7),R.drawable.rules));
        data.add(new DrawerItem(getString(R.string.item_title8),getString(R.string.item_descrip_8),R.drawable.more));
        //data.add(new DrawerItem(getString(R.string.logout),getString(R.string.item_descrip_8),R.drawable.more));

        return data;

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (Constant.isInternetConnected(this)){}
        else{
           // showDialogForNoConnection("No Internet Connection",getString(R.string.no_internet),1);
           // showDialogForNoConnection("No Internet Connection",getString(R.string.no_internet),1);

        }
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(new NetworkBroadcast(),
                   new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
      /* final Account_Info account_info = DroidPrefs.get(this,"account_info",Account_Info.class);

        if (account_info.balance!=null && !account_info.balance.equals("null")) {
            walletMoney.setText("£ "+account_info.balance);
        }
        else{

            walletMoney.setText(" ");

        }*/



    }

    public void getUserbyPhonenumber(){
        String url= APIConstantsDemo.BASE_URL;
        Log.i("url",url);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching userinfo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if(response!=null){
                    try {
                        //JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        if (response.getString("status").equalsIgnoreCase("Success")) {
                             IS_CHECK_DRAWER=true;
                            //  Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();
                            JSONArray jsonarray = response.getJSONArray("data");
                            if (jsonarray != null && jsonarray.length() > 0) {
                                for(int i=0;i<jsonarray.length();i++) {
                                    JSONObject jsonObject1 = jsonarray.getJSONObject(i);
                                    ProductItems productItems = new ProductItems();


                                    productItems.productName = jsonObject1.getString("product_name");
                                    productItems.productDescription = jsonObject1.getString("product_description");
                                    productItems.productImage=jsonObject1.getString("raffle_product_image");
                                    //  productItems.productWinningPrice = jsonObject1.getString("product_winning_price");
                                    productItems.aboutRaffleProvider = jsonObject1.getString("about_raffle_provider");
                                    productItems.raffleProvider = jsonObject1.getString("raffle_provider");
                                    productItems.raffleProviderWeblink = jsonObject1.getString("raffle_provider_weblink");
                                    productItems.address = jsonObject1.getString("address");
                                    productItems.createdOn = jsonObject1.getString("created_on");
                                    productItems.raffleEndCondition = jsonObject1.getString("raffle_end_condition");
                                    productItems.tokenPrice = jsonObject1.getString("token_price");
                                    productItems.totalToken = jsonObject1.getString("total_token");
                                    productItems.deliveryOption = jsonObject1.getString("delivery_option");
                                    productItems.modifiedOn = jsonObject1.getString("modified_on");
                                    productItems.status = jsonObject1.getString("status");
                                    productItems.productId = jsonObject1.getString("raffle_product_id");
                                    productItems.soldToken = jsonObject1.getString("sold_token");
                                    productItems.freeToken = jsonObject1.getString("available_token");
                                    productItems.providerImage = jsonObject1.getString("provider_image");



//                                    JSONArray userRaffleMaps = jsonObject1.optJSONArray("user_raffle_maps");
//                                    JSONArray product_images = jsonObject1.optJSONArray("raffle_product_images");
//
//                                    productItems.productInfoList=new ArrayList<>();
//                                    if(userRaffleMaps!=null && userRaffleMaps.length()>0){
//                                        for(int j=0;j<userRaffleMaps.length();j++){
//
//                                            ProductInfo productInfo=new ProductInfo();
//
//                                            JSONObject jsonObject2=userRaffleMaps.getJSONObject(j);
//                                            productInfo.id=jsonObject2.getString("id");
//                                            productInfo.user_id=jsonObject2.getString("user_id");
//                                            productInfo.raffalProductId=jsonObject2.getString("raffle_product_id");
//                                            productInfo.NoOfTokens=jsonObject2.getString("no_of_token");
//                                            productInfo.transactionId=jsonObject2.getString("transaction_id");
//                                            productInfo.status=jsonObject2.getString("status");
//                                            productInfo.createdOn=jsonObject2.getString("created_on");
//                                            productItems.productInfoList.add(productInfo);
//
//
//
//                                        }
//                                    }
//                                    productItems.productImages=new ArrayList<>();
//                                    if(product_images!=null&& product_images.length()>0){
//                                        for(int k=0;k<product_images.length();k++) {
//                                            ProductImage productImage = new ProductImage();
//                                            JSONObject jsonObj = product_images.getJSONObject(k);
//                                            productImage.id = jsonObj.getString("id");
//                                            productImage.raffallProductId = jsonObj.getString("raffle_product_id");
//                                            productImage.image = jsonObj.getString("image");
//                                            productImage.createdOn = jsonObj.getString("created_on");
//                                            productImage.modifiedOn = jsonObj.getString("modified_on");
//                                            productItems.productImages.add(productImage);
//                                        }
//
//                                    }
//
                                    productItemsList.add(productItems);
                                }





                            }
//                            Toast.makeText(MainActivity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();

                            if(productItemsList.size()>0){
                                swipe_deck_adapter = new Swipe_Deck_Adapter(MainActivity.this);
                                swipe_deck_adapter.addAll(productItemsList);
                                swipeDeck.setAdapter(swipe_deck_adapter);
                            }

                        }
                        else if(response.getString("status").equalsIgnoreCase("authentication_failed")){
                            showDialogForNoConnection("User Authuentication",response.getString("message"),2);

                        }


                    }
                    catch (JSONException e) {
                        Log.e("exp",""+e);
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Please try again later ", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("err",""+error);
                /*try {
                    String responseBody = new String( error.networkResponse.data, "utf-8" );
                    JSONObject jsonObject = new JSONObject( responseBody );
                    Log.i("jsonobject",jsonObject.toString());

                    if(jsonObject!=null){

//                            Intent intent=new Intent(signupActivity, ExistingUser.class);
//                            intent.putExtra("number",edit_telephone.getText().toString());
//                            startActivity(intent);

                       // Snackbar.make(name,jsonObject.getString("message"), Snackbar.LENGTH_SHORT).show();
//                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        error_message.setText(jsonObject.getString("message"));
//                        error_message.setVisibility(View.VISIBLE);
//                        handler=new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                              error_message.setVisibility(View.GONE);
//                            }
//                        },2500);
                    }


                } catch ( JSONException e ) {
                    e.printStackTrace();
                    //Handle a malformed json response
                } catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }*/
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));
    }


    private void paginate() {
        swipeDeck.setPaginationReserved();
        swipe_deck_adapter.addAll(productItemsList);
        swipe_deck_adapter.notifyDataSetChanged();
    }
    public void logout(){
        String url= APIConstantsDemo.LOGOUT_URL;
        Log.i("url",url);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_id",userInfo.userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){


                            DroidPrefs.getDefaultInstance(MainActivity.this).clear();
                            Toast.makeText(MainActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(MainActivity.this,Splash_Activity.class);
                            startActivity(intent);
                            finish();



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

    public void getAccountInfo() {
        String url = APIConstantsDemo.ACCOUNT_URL;
        Log.i("url", url);
        /*progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        JSONObject jsonObject = new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               // progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            JSONObject jsonObj = response.getJSONObject("data");

                            Account_Info account_info=new Account_Info();


                            account_info.id = jsonObj.getString("id");
                            account_info.userId = jsonObj.getString("user_id");
                            account_info.balance = jsonObj.getString("balance");
                            account_info.noOfReferrals = jsonObj.getString("no_of_referrals");
                            account_info.referralRevenueEarned = jsonObj.getString("referral_revenue_earned");
                            account_info.createdOn = jsonObj.getString("referral_revenue_earned");

                            if (account_info.balance!=null && !account_info.balance.equals("null")) {
                                walletMoney.setText("£ "+account_info.balance);
                            }
                            else{

                                walletMoney.setText(" ");

                            }



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            //progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    public void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.logout_dialog, null);
        builder.setView(view);

       // logoutTxt = (TextView) view.findViewById(R.id.logoutText);
        buttonYes = (TextView) view.findViewById(R.id.buttonYes);
        buttonNo = (TextView) view.findViewById(R.id.buttonNo);

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               logout();

            }
        });
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }
    public  void clickPlayButton(){
        int index=swipeDeck.getTopIndex();
        Intent intent=new Intent(MainActivity.this, Question_Activity.class);
        int position;
        if(num_times==0){
            position=index;
        }
        else {
            if(index>=productItemsList.size()){

                position=index-productItemsList.size()*num_times;
                if(position==-1){
                    position=productItemsList.size()-1;
                }
                else if(position==-2){
                    position=productItemsList.size()-2;
                }
            }
            else {
                position=index;
            }
        }
//                if(index>=productItemsList.size()){
//                    position=index-productItemsList.size();
//                }
//                else {
//                    position=index;
//                }
        intent.putExtra("productID",productItemsList.get(position).productId);
        intent.putExtra("freeTicket",productItemsList.get(position).freeToken);
        String total=productItemsList.get(position).totalToken.replaceAll(",","");
        intent.putExtra("totalTicket",total);

        startActivity(intent);
    }
    public void showDialogForNoConnection(String title, String message, final int val) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.ticket_confirm_dialog, null);
        builder.setView(view);

txtHead=(TextView)view.findViewById(R.id.textHeadline);
        ok = (TextView) view.findViewById(R.id.ok);
noConnection=(TextView)view.findViewById(R.id.txtForNoConnection);
noConnection.setText(message);
txtHead.setText(title);
//txtHead.setVisibility(View.GONE);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (val==1){
                  finish();
                }
                else if(val==2){
                    DroidPrefs.getDefaultInstance(MainActivity.this).clear();
                    Intent intent=new Intent(MainActivity.this,Splash_Activity.class);
                    startActivity(intent);
                    finish();
                }
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }

    }
