package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.crashlytics.android.Crashlytics;
import com.example.viewpagerindicator.CirclePageIndicator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.yugasa.raffall.Adapters.SplashAdapter;
import com.yugasa.raffall.Constant;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.RecentToken;
import com.yugasa.raffall.Utils.SplashItem;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramSession;
import net.londatiga.android.instagram.InstagramUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by yugasalabs-45 on 25/8/17.
 */

public class Splash_Activity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private static ViewPager mPager;
    ImageView loginFb, loginTwitter, loginInsta, loginGoogle;
    CallbackManager callbackManager;
    List<String> permissions = new ArrayList<>();
    TextView termsText,policyTxt,ok,noConnection,txtHead;
    String id, name, email,username, mobileno, link, friend_referral_code, login_platform,location;
    Bitmap default_bitmap;
    SharedPreferences sharedPreferences;
    List<UserInfo> userInfoList;
    String base64;
    Dialog dialog;
    private TwitterAuthClient twitterAuthClient;
    AccessToken accesstoken;
    ProgressDialog mProgressDialog;
    private GoogleApiClient mGoogleApiClient;
    private GoogleApiClient client;
    private static final int RC_SIGN_IN = 9001;
    private static final String TWITTER_KEY ="iv3sQM3iAKoprrneSu3I7EM1q";
    private static final String INSTA_KEY ="b207cda048a743179120f58f8d038101";
    private static final String TWITTER_SECRET ="BLnu2jgddzqsn8hGPBtNBRPRsHnkSubhMiHwPjZuVLyUQWxWhI";
    private static final String INSTA_SECRET ="82b1029aacf64cb0843b17714ed4dd11";
    private static final String REDIRECT_URI ="http://isglobal.co/";
    private static final String CALLBACK_URL ="instagram://connect";
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private String TAG = null;
    private static final Integer[] IMAGES = {R.drawable.splash_1, R.drawable.splash_2, R.drawable.spalsh_3, R.drawable.spalsh_4, R.drawable.splash_5};
    private ArrayList<SplashItem> ImagesArray;

    private InstagramSession mInstagramSession;
    private Instagram mInstagram;

    private ProgressBar mLoadingPb;
    private GridView mGridView;
    Uri googleUri;
    int value=0;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInstagram          = new Instagram(this, INSTA_KEY, INSTA_SECRET, REDIRECT_URI);
        mInstagramSession   = mInstagram.getSession();
        setContentView(R.layout.splash_activity);
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(
                TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig)
        );
        twitterAuthClient = new TwitterAuthClient();

        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(this);

//       AppEventsLogger.activateApp(this);

        init();
        userInfoList = new ArrayList<>();
        generatehash();

        loginFb = (ImageView) findViewById(R.id.signIn_fb);
        loginTwitter = (ImageView) findViewById(R.id.signIn_twitter);
        loginInsta = (ImageView) findViewById(R.id.signIn_insta);
        loginGoogle = (ImageView) findViewById(R.id.signIn_google);
        termsText = (TextView) findViewById(R.id.terms);
        policyTxt = (TextView) findViewById(R.id.policy);
        callbackManager = CallbackManager.Factory.create();
        loginGoogle.setOnClickListener(this);

        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Splash_Activity.this,TermsAndCoditionActivity.class);
                startActivity(intent);
            }
        });

        policyTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Splash_Activity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
            }
        });


//instagram login-------------------------------------------------------------------------

        loginInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInstagram.authorize(mAuthListener);

            }
        });


        // twitter sign In Option-----------------------------------------------------------------




        loginTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 boolean isAppInstalled = appInstalledOrNot("com.twitter.android");
               if (isAppInstalled) {
                   twitterAuthClient.authorize(Splash_Activity.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                       @Override
                       public void success(Result<TwitterSession> twitterSessionResult) {
                           // Success
                           login(twitterSessionResult);
                       }

                       @Override
                       public void failure(TwitterException e) {
                           e.printStackTrace();
                       }
                   });

               }
               else {

                  Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.twitter.android"));
                  startActivity(intent);
               }
            }
        });





        //Google sign in option
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        callbackManager = CallbackManager.Factory.create();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();




        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                String userId = accessToken.getUserId();

                getFacebookData(accessToken);
                Toast.makeText(Splash_Activity.this, "successful", Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onCancel() {
                Log.e("error", "error");
            }
            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                        Log.e("ggg", "error"+error);
                    }
                }
            }
        });


        loginFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fblogin();

            }
        });

    }
    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        TwitterSession session = result.data;

        //Getting the username from session
        final String username = session.getUserName();
      //  Toast.makeText(Splash_Activity.this, "" + username, Toast.LENGTH_SHORT).show();
        //This code will fetch the profile image URL
        //Getting the account service of the user logged in

        Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {
                    @Override
                    public void failure(TwitterException e) {
                        //If any error occurs handle it here
                        Toast.makeText(Splash_Activity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void success(Result<User> userResult) {
                        //If it succeeds creating a User object from userResult.data
                        User user = userResult.data;

                        //Getting the profile image url
                        String profileImage = user.profileImageUrl.replace("_normal", "");
                        name = user.name;
                        email=user.email;

                        id=user.idStr;
                        login_platform="twitter";

                      location= user.location;




                        googleUri=Uri.parse(profileImage);
                        new Async().execute();


                       // Toast.makeText(Splash_Activity.this, "" + profileImage + " " + name, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void fblogin() {
        if (Constant.isInternetConnected(this)) {
            permissions.add("email");
            permissions.add("public_profile");
            LoginManager.getInstance().logInWithReadPermissions(this, permissions);
        } else {
            Toast.makeText(Splash_Activity.this, "This feature requires internet connection please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
    public String getimage(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return base64;
    }
    private void getFacebookData(AccessToken accessToken) {
        if (Constant.isInternetConnected(this)){
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(final JSONObject object, GraphResponse response) {
                            System.out.println("object = " + object);
//                            Log.e("myname", object.toString());
                            try {
                                id = object.getString("id");
                                name = object.getString("name");
                                email = object.optString("email");
                               // location = object.getString("address");


                               /* String address=object.getString("last_name");
                               String gender=object.getString("gender");
                                mobileno=object.getString("link");
                                Toast.makeText(Splash_Activity.this, ""+mobileno+" "+address+" "+gender, Toast.LENGTH_SHORT).show();
*/

                                new Asynctask().execute(object.getString("id"), object.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (object.has("birthday")) {
                                try {
                                    Log.e("birthday", object.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,age_range");
            request.setParameters(parameters);
            request.executeAsync();
        }else {
            Toast.makeText(this,"This feature requires internet connection  please check your internet connection",Toast.LENGTH_SHORT).show();
        }



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


       // if(TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);

    // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);


     if (requestCode == RC_SIGN_IN) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        handleSignInResult(result);


    }
}

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("signingoogle", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            Toast.makeText(Splash_Activity.this, "Success", Toast.LENGTH_SHORT).show();
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();


//            Glide.with(getApplicationContext()).load(googleUri)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(default_bitmap);
             login_platform="gmail";
             email=acct.getEmail();
            name=acct.getDisplayName();
            id=acct.getId();

            if(acct.getPhotoUrl()!= null){
                googleUri=acct.getPhotoUrl();
                new Async().execute();

            }else{
                 regis(email,name,login_platform,id, location);
            }





          //  regis(acct.getEmail(), acct.getDisplayName());



        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
            Toast.makeText(Splash_Activity.this, "Failed ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Constant.isInternetConnected(this)){}
        else{
            showDialogForNoConnection();

        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.yugasa.raffall/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.yugasa.raffall/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
        } else {
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signIn_google:
                signIn();
                break;
            // ...
        }
    }

    private void init() {
        ImagesArray = new ArrayList<>();

//            ImagesArray.add(new SplashItem("ab",R.drawable.vpi__tab_indicator));
//            ImagesArray.add(new SplashItem("cd",R.drawable.vpi__tab_indicator));
//            ImagesArray.add(new SplashItem("ds",R.drawable.vpi__tab_indicator));
//            ImagesArray.add(new SplashItem("df",R.drawable.vpi__tab_indicator));
//            ImagesArray.add(new SplashItem("ff",R.drawable.vpi__tab_indicator));

        for(int i = 0; i < IMAGES.length; i++)
        {
            ImagesArray.add(new SplashItem(getResources().getStringArray(R.array.splash)[i],IMAGES[i]));
        }


        mPager = (ViewPager) findViewById(R.id.viewpager);


        mPager.setAdapter(new SplashAdapter(this, ImagesArray));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);


        NUM_PAGES = ImagesArray.size();


    }




    public class Asynctask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Splash_Activity.this);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            URL imageURL = null;
            try {
                imageURL = new URL("https://graph.facebook.com/" + params[0] + "/picture?type=normal");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute( Bitmap bitmap) {
            super.onPostExecute(bitmap);
       default_bitmap=bitmap;
        regis(email,name,"facebook", id,location);
            mProgressDialog.dismiss();
        }


}

    public class Async extends AsyncTask<Void,Void,Bitmap>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Splash_Activity.this);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            Bitmap bitmap=null;

            bitmap=loadBitmap(googleUri.toString());


            return bitmap;
        }
        @Override
        protected void onPostExecute( Bitmap bitmap) {
            super.onPostExecute(bitmap);
            default_bitmap=bitmap;
            regis(email,name,login_platform,id, location);
            mProgressDialog.dismiss();
        }
    }

    private void regis(final String email, final String name, String login_platform, String id, String location) {
        if (Constant.isInternetConnected(this)) {
            String url = APIConstantsDemo.LOGIN_URL;
            Log.i("url", url);
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Registering...");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject jsonObj = new JSONObject();

            try {
                jsonObj.put("name",name);
                if(email!=null && !email.equals("null")){
                jsonObj.put("email",email);
                }
                else{
                    jsonObj.put("email","");
                }

                if (default_bitmap!=null) {
                    jsonObj.put("image", getimage(default_bitmap));
                }else {
                    jsonObj.put("image","");
                }

                jsonObj.put("mobile_no","");
                  if (login_platform.equalsIgnoreCase("facebook")){
                      jsonObj.put("facebook_id",id);
                  }
                  else{
                      jsonObj.put("facebook_id","");
                  }

                if (login_platform.equalsIgnoreCase("twitter")){
                    jsonObj.put("twitter_id",id);
                }
                else{
                    jsonObj.put("twitter_id","");
                }
                if (login_platform.equalsIgnoreCase("gmail")){
                    jsonObj.put("gmail_id",id);
                }
                else{
                    jsonObj.put("gmail_id","");
                }
                if (login_platform.equalsIgnoreCase("instagram")){
                    jsonObj.put("instagram_id",id);
                }
                else{
                    jsonObj.put("instagram_id","");
                }



                jsonObj.put("login_platform",login_platform);

                if(location!=null && !location.equals("null")){
                jsonObj.put("address",location);
                }
                else{jsonObj.put("address","");
                }
                String token=FirebaseInstanceId.getInstance().getToken();
                if(token!=null) {
                    jsonObj.put("device_id",token );
                }
                else{
                    jsonObj.put("device_id","sdfdsgsdgfd" );
                }
                jsonObj.put("device_type","android");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response!=null) {
                        Log.d("respose",""+response);
                        try {
                        if(response.getString("status").equalsIgnoreCase("success")){

//                        Toast.makeText(Splash_Activity.this, "register Successfully", Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(Splash_Activity.this, MainActivity.class));
//                        finish();
                    progressDialog.dismiss();

//                        DroidPrefs.commit(Splash_Activity.this,"id",id);
//                       DroidPrefs.commit(Splash_Activity.this,"name",name);
//                       DroidPrefs.commit(Splash_Activity.this,"email",email);
//                       DroidPrefs.commit(Splash_Activity.this,"image",getimage(default_bitmap));



                            //JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            if(response.getString("user_exist_status").equalsIgnoreCase("1")){
                                JSONObject jsonobj = response.getJSONObject("data");
                                if (jsonobj != null && jsonobj.length() > 0) {
                                    //JSONObject jsonObject1 = jsonobj.getJSONObject(0);
                                    UserInfo user = new UserInfo();


                                    user.name = jsonobj.getString("name");
                                    user.email = jsonobj.getString("email");
                                    user.profile_image = jsonobj.getString("image");
                                    user.mobileNo = jsonobj.getString("mobile_no");
                                    user.loginPlatform = jsonobj.getString("login_platform");
                                    user.address=jsonobj.getString("address");
                                    user.facebookId=jsonobj.getString("facebook_id");
                                    user.twitterId=jsonobj.getString("twitter_id");
                                    user.gmailId=jsonobj.getString("gmail_id");
                                    user.instaId=jsonobj.getString("instagram_id");
                                    user.userId=jsonobj.getString("user_id");
                                    user.deviceType=jsonobj.getString("device_type");
                                    user.deviceId=jsonobj.getString("device_id");
                                    user.userToken=jsonobj.getString("user_token");


                                    DroidPrefs.apply(Splash_Activity.this,"user_info",user);
//
                                    user=DroidPrefs.get(Splash_Activity.this,"user_info",UserInfo.class);

                                   // Toast.makeText(Splash_Activity.this, user.name, Toast.LENGTH_SHORT).show();
//                                    userInfoList.add(user);
                                    //  Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();

                                }
                                //  Toast.makeText(Splash_Activity.this, "list"+userInfoList.size(), Toast.LENGTH_SHORT).show();


//                                    Log.i("userinfo",userInfoList.get(0).email);
                                Intent i=new Intent(Splash_Activity.this,Refferal_Code_Activity.class);
                                //   i.putParcelableArrayListExtra("user_info", (ArrayList<? extends Parcelable>) userInfoList);
                                startActivity(i);
                                finish();
                            }
                          else  if (response.getString("user_exist_status").equalsIgnoreCase("0")) {
                          // Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();
                                JSONObject jsonobj = response.getJSONObject("data");
                                if (jsonobj != null && jsonobj.length() > 0) {
                                    //JSONObject jsonObject1 = jsonobj.getJSONObject(0);
                                    UserInfo user = new UserInfo();


                                    user.name = jsonobj.getString("name");
                                    user.email = jsonobj.getString("email");
                                    user.profile_image = jsonobj.getString("image");
                                    user.mobileNo = jsonobj.getString("mobile_no");
                                    user.loginPlatform = jsonobj.getString("login_platform");
                                    user.address=jsonobj.getString("address");
                                    user.facebookId=jsonobj.getString("facebook_id");
                                    user.twitterId=jsonobj.getString("twitter_id");
                                    user.gmailId=jsonobj.getString("gmail_id");
                                    user.instaId=jsonobj.getString("instagram_id");
                                    user.userId=jsonobj.getString("user_id");
                                    user.deviceType=jsonobj.getString("device_type");
                                    user.deviceId=jsonobj.getString("device_id");
                                    user.userToken=jsonobj.getString("user_token");


                                    DroidPrefs.apply(Splash_Activity.this,"user_info",user);
//
                                    user=DroidPrefs.get(Splash_Activity.this,"user_info",UserInfo.class);

                                   // Toast.makeText(Splash_Activity.this, user.name, Toast.LENGTH_SHORT).show();
//                                    userInfoList.add(user);
                                  //  Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();

                                }
                              //  Toast.makeText(Splash_Activity.this, "list"+userInfoList.size(), Toast.LENGTH_SHORT).show();


//                                    Log.i("userinfo",userInfoList.get(0).email);
                                    Intent i=new Intent(Splash_Activity.this,MainActivity.class);
                                 //   i.putParcelableArrayListExtra("user_info", (ArrayList<? extends Parcelable>) userInfoList);
                                    startActivity(i);
                                    finish();


                            }



                        }
                        else if(response.getString("status").equalsIgnoreCase("success")
                                ){
                            Toast.makeText(Splash_Activity.this, ""+response.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                        else if(response.getString("status").equalsIgnoreCase("failed")){
                                Toast.makeText(Splash_Activity.this, ""+response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        }
                         catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(Splash_Activity.this, "Please try again later ", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Log.d("volleyerrot",""+error);
                }
            });
            MySingleton.getInstance(this).addToRequestQueue(jsonRequest);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                    DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));
           //////////////////////////////////////////////////////////////////////////////

        }
    }




//    public void register(final String email, final String name){
//        if (Constant.isInternetConnected(this)){
//            String url = Apis.BASE_URL+"facebook-login";
//            Log.i("url", url);
//            final ProgressDialog progressDialog=new ProgressDialog(this);
//            progressDialog.setMessage("Registering...");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//
//            JSONObject jsonObject=new JSONObject();
//            try {
//                jsonObject.put("name",name);
//                jsonObject.put("email",email);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
//
//            StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//
//                @Override
//                public void onResponse(String response) {
//                    progressDialog.dismiss();
//                    if(response!=null){
//                        try {
//                            JSONObject jsonObject=new JSONObject(response);
//                            if(jsonObject.getString("status").equalsIgnoreCase("1") ||jsonObject.getString("status").equalsIgnoreCase("2")){
////                            Toast.makeText(StartScreen.this, "Successfully login. Welcome to CashCase.", Toast.LENGTH_SHORT).show();
//                                JSONArray jsonArray=jsonObject.getJSONArray("Detail");
//                                if(jsonArray!=null && jsonArray.length()>0) {
//                                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
//                                    UserInfo user = new UserInfo();
//
//
//
//                                    user.name = jsonObject1.getString("name");
//                                    user.email = jsonObject1.getString("email");
//                                    user.mobileNo = jsonObject1.getString("mobile no");
//                                    user.Profile_image = jsonObject1.getString("profile image");
//                                    user.link = jsonObject1.getString("link");
//                                    user.friendReferralCode= jsonObject1.getString("friend_referral_code");
//
//                                    user.loginPlatform = jsonObject1.getString("login_platform");
//                                    user.isfbsignup = true;
//
//
//                                    DroidPrefs.apply(Splash_Activity.this,"user",user);
//
//
//                                        Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to CashCase.", Toast.LENGTH_SHORT).show();
//                                        startActivity(new Intent(Splash_Activity.this,Home_Activity.class));
//                                        finish();
//
//
//
//
//
//
//
//                                    finish();
//                                }
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(Splash_Activity.this, "Please try again later ", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    progressDialog.dismiss();
//                    Log.d("Volly error",""+error);
//                    Toast.makeText(Splash_Activity.this, "Please try again later "+error, Toast.LENGTH_SHORT).show();
//                }
//
//            } ) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    HashMap<String,String> params=new HashMap<>();
//                    params.put("name",name);
//                    params.put("email",email);
//
//                    return params;
//                }
//            };
//
//            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
//            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
//                    DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));
//        }else {
//            Toast.makeText(this,"This feature requires internet connection  please check your internet connection",Toast.LENGTH_SHORT).show();
//        }
//    }
public Bitmap loadBitmap(String uri)
{
    Bitmap bm = null;
    InputStream is = null;
    BufferedInputStream bis = null;
    try
    {
        URL url=new URL(uri);
        URLConnection conn = url.openConnection();
        conn.connect();
        is = conn.getInputStream();
        bis = new BufferedInputStream(is, 8192);
        bm = BitmapFactory.decodeStream(bis);
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
    finally {
        if (bis != null)
        {
            try
            {
                bis.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (is != null)
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    return bm;
}









    public void generatehash(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.yugasa.raffall",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", "KeyHash:"+ Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
               // Toast.makeText(getApplicationContext(), Base64.encodeToString(md.digest(),
                      //  Base64.DEFAULT), Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
       String token= FirebaseInstanceId.getInstance().getToken();
        if(token!=null) {

            Log.i("token", token);
            RecentToken recentToken1 = new RecentToken();
            recentToken1.token=token;
            Log.i("token",recentToken1.token);
            DroidPrefs.apply(this, "token", recentToken1);
        }
    }

    private Instagram.InstagramAuthListener mAuthListener = new Instagram.InstagramAuthListener() {
        @Override
        public void onSuccess(InstagramUser user) {
           // showToast(user.fullName);

            regis(user.username,user.fullName,"instagram",user.id,"");
        }

        @Override
        public void onError(String error) {
            showToast(error);
        }

        @Override
        public void onCancel() {
            showToast("cancel");
        }
    };


    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }


    public void showDialogForNoConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Splash_Activity.this);
        View view = getLayoutInflater().inflate(R.layout.ticket_confirm_dialog, null);
        builder.setView(view);

        txtHead=(TextView)view.findViewById(R.id.textHeadline);
        ok = (TextView) view.findViewById(R.id.ok);
        noConnection=(TextView)view.findViewById(R.id.txtForNoConnection);
        noConnection.setText(R.string.no_internet);
        txtHead.setVisibility(View.GONE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
}
