package com.yugasa.raffall.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.example.viewpagerindicator.CirclePageIndicator;
import com.yugasa.raffall.Adapters.Product_Image_Adapter;
import com.yugasa.raffall.Application;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.ProductDetails;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.Items.ProductImage;
import com.yugasa.raffall.Utils.ProductItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class Card_ItemActivity extends AppCompatActivity {
    TextView productDescription,raffalProvider,productName,ticketPrice,totalTicket,soldTicket,freeTicket,country,productDelivery,productEnds,toolbarTitle,raffalCountHistory,companyName;
    ImageView productLogo,backIcon,soldTickets,history,shareIcon;
    Toolbar toolbar;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    int seconds=0,minits=0,hours=0,days=0;
    Button play;
    String dateFormat;
    String outputText;
    ArrayAdapter arrayAdapter;
    ProductDetails pdetails;
    List<ProductItems> list;
    List<ProductImage> listImage;
    List<String> ProductImages;
    ViewPager mPager;
    Intent intent;
    RatingBar ratingBar;
    ListView listView;
    Dialog dialog;
    String freetoken;
    String msg1=" WOW did you see the Raffl Ticket app is giving away the ";
    String msg2=".Click here to find out more ";
   public int pos;
    ProgressDialog progressDialog;

    ImageLoader imageLoader;
    String product_id;
    String image_url;
    Bitmap bitmap;
    File image_name;

    Product_Image_Adapter product_image_adapter;

    public Card_ItemActivity() throws ParseException {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card__item);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbarTitle=(TextView) findViewById(R.id.toolbarTitle);
        companyName=(TextView) findViewById(R.id.productBrand);
        shareIcon=(ImageView) findViewById(R.id.share);
        raffalCountHistory=(TextView) findViewById(R.id.countRaffalHistory);
        backIcon=(ImageView) findViewById(R.id.back_icon);
        play=(Button) findViewById(R.id.playPuzzle);
        soldTickets=(ImageView) findViewById(R.id.soldticket);
        history=(ImageView) findViewById(R.id.history);
       toolbarTitle.setText("Open Ended");
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = { Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        soldTickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Card_ItemActivity.this,SoldTicketActivity.class);
                intent.putExtra("r_p_Id",pdetails.raffalproductId);
                startActivity(intent);
            }
        });

        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  showDialog();
                sendData(msg1+pdetails.productName+msg2);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Card_ItemActivity.this,Raffle_HistoryActivity.class);
                intent.putExtra("r_p_Id",pdetails.raffalproductId);
                startActivity(intent);
            }
        });
        companyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCompanyDetailsDialog();
            }
        });



        listImage=new ArrayList<>();

        imageLoader= MySingleton.getInstance(this).getImageLoader();
        list=new ArrayList<ProductItems>();
        ProductImages=new ArrayList<>();

        intent=getIntent();
        if (intent!=null){
            if(intent.getIntExtra("type",-1)==1){
                badgeCount();

            }

        }

        if(intent!=null){

         product_id=intent.getStringExtra("productID");
         getProduct(product_id);

        }

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Card_ItemActivity.this, Question_Activity.class);
                intent.putExtra("productID",product_id);
                String total=totalTicket.getText().toString().replaceAll(",","");
                intent.putExtra("totalTicket",total);

                intent.putExtra("freeTicket",freeTicket.getText().toString());
                startActivity(intent);
            }
        });

         product_image_adapter=new Product_Image_Adapter(this,ProductImages);
        mPager = (ViewPager) findViewById(R.id.viewpager);
        mPager.setAdapter(product_image_adapter);




        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(5 * density);



       // NUM_PAGES = ProductImages.size();
        productDescription=(TextView)findViewById(R.id.product_descrip);
        productName=(TextView)findViewById(R.id.productname);
        ticketPrice=(TextView)findViewById(R.id.ticketPrice);
        totalTicket=(TextView)findViewById(R.id.totalTicket);
        soldTicket=(TextView)findViewById(R.id.totalSold);
        freeTicket=(TextView)findViewById(R.id.totalFree);
        productLogo=(ImageView) findViewById(R.id.product_logo);
        raffalProvider=(TextView)findViewById(R.id.productBrand);
        country=(TextView)findViewById(R.id.counryName);
        productDelivery=(TextView)findViewById(R.id.product_delivery);
        productEnds=(TextView)findViewById(R.id.product_ends);
        ratingBar=(RatingBar) findViewById(R.id.ratingBar);




    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Application.is_netconnected==false){
            displayAlert();
        }
    }
    private void displayAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getApplicationContext());
        builder.setMessage("No Internet Connection").setCancelable(
                false).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int pid = android.os.Process.myPid();
                        android.os.Process.killProcess(pid);
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        startActivity(intent);

                        dialog.dismiss();


                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void getProduct(String product_id){
        String url=APIConstantsDemo.PRODUCT_INFO_URL;
        Log.i("url", url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
              progressDialog=new ProgressDialog(this);
            progressDialog.setMessage("Fetching productinfo...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("raffle_product_id",product_id);
                jsonObject.put("user_token",userInfo.userToken);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("Success")){

                            JSONObject jsonObj=response.getJSONObject("data");
                             pdetails=new ProductDetails();
                            pdetails.productName=jsonObj.getString("product_name");
                            pdetails.productDescription=jsonObj.getString("product_description");
                           // pdetails.productImages=jsonObj.getString("raffle_product_images");
                            pdetails.address=jsonObj.getString("address");
                            pdetails.providerImage=jsonObj.getString("provider_image");
                            pdetails.aboutRaffleProvider=jsonObj.getString("about_raffle_provider");
                            pdetails.raffalproductId=jsonObj.getString("raffle_product_id");
                            pdetails.raffleProvider=jsonObj.getString("raffle_provider");
                            pdetails.raffleProviderWeblink=jsonObj.getString("raffle_provider_weblink");
                            pdetails.deliveryOption=jsonObj.getString("delivery_option");
                            pdetails.status=jsonObj.getString("status");
                            pdetails.soldToken=jsonObj.getString("sold_token");
                            pdetails.freeToken=jsonObj.getString("available_token");
                            pdetails.tokenPrice=jsonObj.getString("token_price");
                            pdetails.totalToken=jsonObj.getString("total_token");
                            pdetails.createdon=jsonObj.getString("created_on");
                            pdetails.modifiedOn=jsonObj.getString("modified_on");
                            pdetails.raffalEndCondition=jsonObj.getString("raffle_end_condition");
                            pdetails.countRaffleHistory=jsonObj.getString("count_raffle_history");
                            pdetails.average_raffle_history_rating=jsonObj.getInt("average_raffle_history_rating");


                            JSONArray jsonArray=jsonObj.optJSONArray("raffle_product_images");
                            pdetails.productImages=new ArrayList<>();
                                if(jsonArray!=null && jsonArray.length()>0){

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ProductImage productImage=new ProductImage();
                                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                        productImage.id=jsonObject1.getString("id");
                                        productImage.image=jsonObject1.getString("image");
                                        productImage.modifiedOn=jsonObject1.getString("modified_on");
                                        productImage.createdOn=jsonObject1.getString("created_on");
                                        productImage.raffallProductId=jsonObject1.getString("raffle_product_id");

                                        pdetails.productImages.add(productImage);

                                    }
                                }

                                if(pdetails.productImages.size()>0){
                                    setItems(pdetails);

                                    for (int i = 0; i < pdetails.productImages.size(); i++) {
                                        ProductImages.add(pdetails.productImages.get(i).image);
                                    }
                                    if (ProductImages.size()>0){
                                        product_image_adapter.notifyDataSetChanged();

                                    }
                                }
                                if(pdetails.raffalEndCondition.equalsIgnoreCase("Ends When all tokens are sold")){

                                }else
                                {
                                    convertDateFormat();
                                }




                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }
    public void setItems(ProductDetails productDetails){

        productDescription.setText(productDetails.productDescription);
        raffalProvider.setText(productDetails.raffleProvider);
        country.setText(productDetails.address);
        productName.setText(productDetails.productName);
        ticketPrice.setText("£ "+productDetails.tokenPrice);
        totalTicket.setText(productDetails.totalToken);
        soldTicket.setText(productDetails.soldToken);
        freeTicket.setText(productDetails.freeToken);
        productDelivery.setText(productDetails.deliveryOption);
        raffalCountHistory.setText(productDetails.countRaffleHistory+ " "+"Item");
            productEnds.setText(productDetails.raffalEndCondition);

            if(pdetails.average_raffle_history_rating>0) {
                ratingBar.setRating(Integer.parseInt(String.valueOf(pdetails.average_raffle_history_rating)));
            }



        raffalProvider.setText(productDetails.raffleProvider);
image_url=productDetails.providerImage;

        imageLoader.get(productDetails.providerImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    bitmap=response.getBitmap();
                    productLogo.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
            progressDialog.dismiss();
            }
        });
    }
    public void showDialog() {
        dialog = new Dialog(this);

       // View v = getLayoutInflater().inflate(R.layout.share_bottomsheet_dialog, null);
        dialog.setContentView(R.layout.share_bottomsheet_dialog);
       // bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());
        listView=dialog.findViewById(R.id.listItem);
        arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.share_on_social));
        listView.setAdapter(arrayAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {


                }
            }
        });
        //shareitems.add(getResources().getStringArray(R.array.share_menu))

      //  bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        dialog.show();


    }
    public void sendData(final String message){

           /*Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
           shareIntent.setType("text/plain");
           shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);

            shareIntent = getPackageManager().getLaunchIntentForPackage(application);
                   shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                   shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

                        startActivity(shareIntent);
*/
        final String shareBody = "https://play.google.com/store/apps/details?id=com.twitter.android";


        //  sharingIntent = getPackageManager().getLaunchIntentForPackage(application);
        progressDialog = new ProgressDialog(Card_ItemActivity.this);
        progressDialog.setMessage("wait....");
        progressDialog.show();
        image_url=ProductImages.get(mPager.getCurrentItem());     //  sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");
        imageLoader = MySingleton.getInstance(Card_ItemActivity.this).getImageLoader();
        imageLoader.get(image_url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    Bitmap scaled_bitmap;
                    int height=response.getBitmap().getHeight();
                    int width=response.getBitmap().getWidth();
                    if(height>500 && width>500){
                        scaled_bitmap=Bitmap.createScaledBitmap(response.getBitmap(),300,300,false);
                    }
                    else{
                        scaled_bitmap=response.getBitmap();
                    }
                    SaveImage(scaled_bitmap,shareBody);

                    progressDialog.dismiss();
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);

                    sendIntent.putExtra(Intent.EXTRA_TEXT,message+"\n"+" "+shareBody );
                    sendIntent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(image_name));
                    sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    sendIntent.setType("image/jpeg");
                    startActivity(Intent.createChooser(sendIntent, "Share URl"));

                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);

                sendIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share URl"));

            }
        });



    }

    public void badgeCount() {
        String url = APIConstantsDemo.BADGE_COUNT;
        Log.i("url", url);
        UserInfo userInfo = DroidPrefs.get(this, "user_info", UserInfo.class);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userInfo.userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
             progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


public void convertDateFormat() {

    SimpleDateFormat inputFormat = new SimpleDateFormat(
            "dd-MM-yyyy HH:mm z");
    inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

//Date date=inputFormat.parse()



    SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");


// Adjust locale and zone appropriately/

    Date date = null;
    try {
        date = inputFormat.parse(pdetails.raffalEndCondition);
    } catch (ParseException e) {
        e.printStackTrace();
    }

    long millisecond = date.getTime();
    Calendar calendar=Calendar.getInstance();
    calendar.setTime(date);
    String month_text;
    String day=calendar.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG,Locale.getDefault());
    int day_month=calendar.get(Calendar.DAY_OF_MONTH);
    String month=calendar.getDisplayName(Calendar.MONTH,Calendar.LONG,Locale.getDefault());
    int year= calendar.get(Calendar.YEAR);
    int hour=calendar.get(Calendar.HOUR_OF_DAY);
    int minutes=calendar.get(Calendar.MINUTE);

    if(day_month==2 ||day_month==22){
        month_text=day_month+"nd";
    }
    else if(day_month==1 || day_month==21 || day_month==31){
        month_text=day_month+"st";
    }
    else if(day_month==3 || day_month==23){
        month_text=day_month+"rd";
    }
    else{
        month_text=day_month+"th";
    }


    dateFormat=day+", "+month_text+" "+month+" "+year+" at "+twoDigitString(hour)+":"+twoDigitString(minutes)+" "+"GMT";
    productEnds.setText(dateFormat);
    long current_millisecond = System.currentTimeMillis();
    long diff = millisecond - current_millisecond;

    if (diff > 0) {
        seconds = (int) (diff / 1000);

        if (seconds > 60) {
            minits = seconds / 60;
        }
        if (minits > 60) {
            hours = minits / 60;
        }
        if (hours > 24) {
            days = hours / 24;
        }
       // if(seconds>)

        new CountDownTimer(diff, 1000)
        {
            public void onTick(long diff)
            {

                /*if (diff > 0) {
                    seconds = (int) (diff / 1000);


                    if (seconds >=60) {
                    minits = seconds / 60;
                }

                if (minits > 60) {
                    hours = minits / 60;
                }
                if (hours > 24) {
                    days = hours / 24;
                }}*/
               // diff    = (int) (diff/1000);
               // toolbarTitle.setText(days+"d "+hours+"h "+minits+"m "+seconds+"s");
                toolbarTitle.setText(formatMilliSecondsToTime(diff));
                //here you can have your logic to set text to edittext } }.start();
            }

            @Override
            public void onFinish() {

            }
        }.start();
//    outputText = outputFormat.format(date);


    }



}

    private String formatMilliSecondsToTime(long milliseconds) {
        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        int days=(int)(milliseconds/(1000*60*60*24));
        return twoDigitString(days)+"d "+twoDigitString(hours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds)+"s";
    }

    private String twoDigitString(long number)
    { if (number == 0)
    { return "00";
    } if (number / 10 == 0)

    { return "0" + number; }


        return String.valueOf(number);
    }

    public void showCompanyDetailsDialog() {
       // productItems=getItem(position);
        bottomSheetDialog = new BottomSheetDialog(this);
        View v = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_dialog_popup, null);
        bottomSheetDialog.setContentView(v);
        bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());

        final TextView webLink=(TextView)v.findViewById(R.id.webLink);
        Button close=(Button)v.findViewById(R.id.clickDialog);
        TextView raffalProvider=(TextView)v.findViewById(R.id.productBrand);
        final CircleImageView productLogo=(CircleImageView)v.findViewById(R.id.productlogo);
        TextView productName=(TextView)v.findViewById(R.id.productName);
        TextView  description=(TextView)v.findViewById(R.id.description);

        description.setText(pdetails.aboutRaffleProvider);
        webLink.setText(Html.fromHtml("<u>"+pdetails.raffleProviderWeblink+"</u>"));
        raffalProvider.setText(pdetails.raffleProvider);
        //Glide.with(getContext()).load(productItems.providerImage).into(productLogo);
        imageLoader.get(pdetails.providerImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    productLogo.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        webLink.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse("http://"+webLink.getText().toString());
                i.setData(uri);
                startActivity(i);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    bottomSheetDialog.dismiss();
                }
            }
        });
        //   bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();
        bottomSheetDialog.setCanceledOnTouchOutside(false);




    }
    private void SaveImage(Bitmap finalBitmap ,String text) {

        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/Raffel_project/"
                        );

        // Make sure the path directory exists.
        if(!path.exists())
        {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, "product.png");



        try {
//            file.createNewFile();
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            image_name=file;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




}