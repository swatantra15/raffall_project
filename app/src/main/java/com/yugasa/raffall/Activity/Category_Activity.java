package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.AccountHistory;
import com.yugasa.raffall.Adapters.AccountHistoryAdapter;
import com.yugasa.raffall.Adapters.Categories_Adapter;
import com.yugasa.raffall.CategoryClass;
import com.yugasa.raffall.CategoryList;
import com.yugasa.raffall.HistoryDetails;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.ProductItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Category_Activity extends AppCompatActivity {
    TextView toolbarTitle;
    UserInfo userInfo;
    RecyclerView recyclerView;
    ImageView backIcon,shareIcon;
    ProgressDialog progressDialog;
   public List<CategoryList> categoryMenuList;
    List<ProductItems> categoryItemList;
    Categories_Adapter categoriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_);

        toolbarTitle=(TextView)findViewById(R.id.toolbarTitle);
        recyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        shareIcon=(ImageView)findViewById(R.id.share);
        toolbarTitle.setText(getString(R.string.item_title1));



        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        shareIcon.setVisibility(View.GONE);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        categoryMenuList=new ArrayList<>();
        categoryItemList=new ArrayList<>();
        getCategoryDetails();





    }
    public void getCategoryDetails(){
        String url= APIConstantsDemo.CATEGORY_URL;
        Log.i("url",url);
      userInfo =DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching userinfo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            JSONArray jsonArray = response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {


                                CategoryList categoryList=new CategoryList();
                                JSONObject json1 = jsonArray.getJSONObject(i);

                                categoryList.categoryName = json1.getString("category_name");
                                categoryList.categoryId= json1.getString("category_id");
                                categoryList.status= json1.getString("status");

                                categoryMenuList.add(categoryList);

                            }
                            if(categoryMenuList.size()>0){
                                CategoryList categoryList=new CategoryList();
                                categoryList.categoryName="All Products";
                                categoryMenuList.add(0,categoryList);
                                categoriesAdapter=new Categories_Adapter(categoryMenuList, Category_Activity.this, new Categories_Adapter.CategoryClick() {
                                    @Override
                                    public void Clicked(View view, int position) {
                                        if(position==0){
                                            Intent intent=new Intent(Category_Activity.this,MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                        else {

                                            getCategoryItems(categoryMenuList.get(position).categoryId);
                                        }

                                    }
                                });
                                recyclerView.setAdapter(categoriesAdapter);
                            }




                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    public void getCategoryItems(String id) {
        String url = APIConstantsDemo.CATEGORY_ITEM;
        Log.i("url", url);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("category_id",id);
            jsonObject.put("user_token",userInfo.userToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {
                            JSONArray jsonArray=response.getJSONArray("data");

                            CategoryClass categoryItem=new CategoryClass();
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                ProductItems productItems=new ProductItems();
//                                categoryItem.productName=jsonObject1.getString("product_name");
//                                categoryItem.productDescription=jsonObject1.getString("product_description");
//                                categoryItem.tokenPrice=jsonObject1.getString("token_price");
//                                categoryItem.totalToken=jsonObject1.getString("total_token");
//                                categoryItem.rafflProvider=jsonObject1.getString("raffle_provider");
//                                categoryItem.aboutRafflProvider=jsonObject1.getString("about_raffle_provider");
//                                categoryItem.rafflProviderWebLink=jsonObject1.getString("raffle_provider_weblink");
//                                categoryItem.providerImage=jsonObject1.getString("provider_image");
//                                categoryItem.address=jsonObject1.getString("address");
//                                categoryItem.raffalEndCondition=jsonObject1.getString("raffle_end_condition");
//                                categoryItem.deliveryOption=jsonObject1.getString("delivery_option");
//                                categoryItem.soldToken=jsonObject1.getString("sold_token");
//                                categoryItem.raffalProductId=jsonObject1.getString("raffle_product_id");
//                                categoryItem.raffalProductImage=jsonObject1.getString("raffle_product_image");
//                                categoryItem.availableToken=jsonObject1.getString("available_token");
//                                categoryItem.categoryId=jsonObject1.getString("category_id");
                                productItems.productName = jsonObject1.getString("product_name");
                                productItems.productDescription = jsonObject1.getString("product_description");
                                productItems.productImage=jsonObject1.getString("raffle_product_image");
                                //  productItems.productWinningPrice = jsonObject1.getString("product_winning_price");
                                productItems.aboutRaffleProvider = jsonObject1.getString("about_raffle_provider");
                                productItems.raffleProvider = jsonObject1.getString("raffle_provider");
                                productItems.raffleProviderWeblink = jsonObject1.getString("raffle_provider_weblink");
                                productItems.address = jsonObject1.getString("address");
                                productItems.createdOn = jsonObject1.getString("created_on");
                                productItems.raffleEndCondition = jsonObject1.getString("raffle_end_condition");
                                productItems.tokenPrice = jsonObject1.getString("token_price");
                                productItems.totalToken = jsonObject1.getString("total_token");
                                productItems.deliveryOption = jsonObject1.getString("delivery_option");
                                productItems.modifiedOn = jsonObject1.getString("modified_on");
                                productItems.status = jsonObject1.getString("status");
                                productItems.productId = jsonObject1.getString("raffle_product_id");
                                productItems.soldToken = jsonObject1.getString("sold_token");
                                productItems.freeToken = jsonObject1.getString("available_token");
                                productItems.providerImage = jsonObject1.getString("provider_image");
                                categoryItemList.add(productItems);

                            }

                           if (categoryItemList.size()>0){
                               Intent intent=new Intent(Category_Activity.this,MainActivity.class);
                               intent.putParcelableArrayListExtra("product_item", (ArrayList<? extends Parcelable>) categoryItemList);
                               startActivity(intent);
                               finish();
                           }


                        }
                        else if (response.getString("status").equalsIgnoreCase("failed")){
                            Toast.makeText(Category_Activity.this, response.getString("message"), Toast.LENGTH_SHORT).show();

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Category_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


}
