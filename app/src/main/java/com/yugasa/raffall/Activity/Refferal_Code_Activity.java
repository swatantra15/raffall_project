package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

public class Refferal_Code_Activity extends AppCompatActivity {
    EditText editText1,editText2,editText3,editText4,editText5,editText6,editText7,editText8;
    TextView continueWithoutRefferal,enter,messageTxt;
ProgressDialog progressDialog;
SharedPreferences sharedPreferences;
    String reffralCode;
    Button ok;
    Dialog dialog;
    boolean flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refferal__code_);
        editText1=(EditText)findViewById(R.id.editText1);
        editText2=(EditText)findViewById(R.id.editText2);
        editText3=(EditText)findViewById(R.id.editText3);
        editText4=(EditText)findViewById(R.id.editText4);
        editText5=(EditText)findViewById(R.id.editText5);
        editText6=(EditText)findViewById(R.id.editText6);
        editText7=(EditText)findViewById(R.id.editText7);
        editText8=(EditText)findViewById(R.id.editText8);
        continueWithoutRefferal=(TextView) findViewById(R.id.continue_without_refferal);
        enter=(TextView) findViewById(R.id.enter);



        sharedPreferences=getSharedPreferences("App_Setting",MODE_PRIVATE);

        sharedPreferences.edit().putBoolean("isShowCheckToolbar",true).commit();



        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reffralCode=editText1.getText().toString()+editText2.getText().toString()+editText3.getText().toString()+editText4.getText().toString()+editText5.getText().toString()+editText6.getText().toString()+editText7.getText().toString()+editText8.getText().toString();
                getFriendRefferal();
                //Toast.makeText(Refferal_Code_Activity.this, "code is  "+reffralCode, Toast.LENGTH_LONG).show();

            }
        });





        continueWithoutRefferal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Refferal_Code_Activity.this,Social_Login_Link.class);
                intent.putExtra("type",0);
                startActivity(intent);
                finish();
            }
        });



        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 // reffralCode="";
                if(s.length()==1){
                    //reffralCode+=s.toString();
                    editText2.setEnabled(true);
                    editText2.requestFocus();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText1.getText().toString().equalsIgnoreCase("")){
                    editText1.requestFocus();}
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==1){
                   // reffralCode+=s.toString();
                    editText3.setEnabled(true);
                    editText3.requestFocus();}
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    editText1.requestFocus();
                }
            }

        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText2.getText().toString().equalsIgnoreCase("")){
                    editText2.requestFocus();}


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==1){
                   // reffralCode+=s.toString();
                    editText4.setEnabled(true);
                    editText4.requestFocus();}
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0){
                    editText2.requestFocus();}

            }
        });
        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText3.getText().toString().equalsIgnoreCase("")){
                    editText3.requestFocus();}
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // reffralCode+=s.toString();
                editText5.setEnabled(true);
                editText5.requestFocus();}


            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0){
                    editText3.requestFocus();}

            }
        });

        editText5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText4.getText().toString().equalsIgnoreCase("")){
                    editText4.requestFocus();}

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // reffralCode+=s.toString();
                editText6.setEnabled(true);
                editText6.requestFocus();}


            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0){
                    editText4.requestFocus();}

            }
        });
        editText6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText5.getText().toString().equalsIgnoreCase("")){
                    editText5.requestFocus();}
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // reffralCode+=s.toString();
                editText7.setEnabled(true);
                editText7.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0){
                    editText5.requestFocus();}

            }
        });

        editText7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText6.getText().toString().equalsIgnoreCase("")){
                    editText6.requestFocus();}

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // reffralCode+=s.toString();
                editText8.setEnabled(true);
                editText8.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==0){
                    editText6.requestFocus();}

            }
        });
        editText8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(editText7.getText().toString().equalsIgnoreCase("")){
                    editText7.requestFocus();}


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //reffralCode+=s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(Refferal_Code_Activity.this, "code is"+reffralCode, Toast.LENGTH_SHORT).show();
                if(s.toString().length()==0){
                    editText7.requestFocus();
                }

            }
        });


    }
    public void getFriendRefferal() {
        String url = APIConstantsDemo.FRIEND_REFFERAL_URL;
        Log.i("url", url);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
            jsonObject.put("friend_referral_code",reffralCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {
                              flag=true;
                           // Toast.makeText(Refferal_Code_Activity.this,response.getString("message"), Toast.LENGTH_SHORT).show();

                           /* dialog = new Dialog(Refferal_Code_Activity.this);
                            // View v = getLayoutInflater().inflate(R.layout.share_bottomsheet_dialog, null);
                            dialog.setContentView(R.layout.friend_refferal_dialog);

                            Intent intent=new Intent(Refferal_Code_Activity.this,Social_Login_Link.class);
                            startActivity(intent);*/
                           showdialog(response.getString("message"));
                        }
                        else
                            if (response.getString("status").equalsIgnoreCase("failed")){
                                flag=false;
                                showdialog(response.getString("message"));
                           // Toast.makeText(Refferal_Code_Activity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    public void showdialog(String message){
AlertDialog.Builder builder=new AlertDialog.Builder(Refferal_Code_Activity.this);
        View view=getLayoutInflater().inflate(R.layout.friend_refferal_dialog,null);
        builder.setView(view);

        messageTxt=(TextView)view.findViewById(R.id.text) ;
        ok=(Button)view.findViewById(R.id.buttonOk) ;
        messageTxt.setText(message);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag){
                    Intent intent=new Intent(Refferal_Code_Activity.this,Social_Login_Link.class);
                    startActivity(intent);
                   dialog.dismiss();
                }
                else{
                    dialog.dismiss();
                }
            }
        });
        dialog=builder.create();

        dialog.show();




    }
}
