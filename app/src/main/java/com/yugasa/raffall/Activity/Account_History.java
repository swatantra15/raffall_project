package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.AccountHistory;
import com.yugasa.raffall.Adapters.AccountHistoryAdapter;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Account_History extends AppCompatActivity {
    TextView toolbarTitle,noDataText;
    ProgressDialog progressDialog;
    ImageView backIcon,shareIcon;
    AccountHistoryAdapter accountHistoryAdapter;
    RecyclerView recyclerView;
    List<AccountHistory> accountHistoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account__history);

        toolbarTitle=(TextView)findViewById(R.id.toolbarTitle);
        noDataText=(TextView)findViewById(R.id.no_data_text);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        shareIcon=(ImageView)findViewById(R.id.share);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);

        accountHistoryList=new ArrayList<>();
        toolbarTitle.setText("Account History");
        shareIcon.setVisibility(View.GONE);

        getAccountHistory();

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));






    }
    public void getAccountHistory() {
        String url = APIConstantsDemo.ACCOUNT_HISTORY;
        Log.i("url", url);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (accountHistoryList.size()>0){
                            accountHistoryList.clear();
                        }
                        if (response.getString("status").equalsIgnoreCase("success")) {
                            JSONArray jsonArray=response.getJSONArray("data");


                            for (int i = 0; i <jsonArray.length() ; i++) {
                                AccountHistory accountHistory =new AccountHistory();
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                accountHistory.id=jsonObject1.getString("id");
                                accountHistory.user_id=jsonObject1.getString("user_id");
                                accountHistory.credit_amount=jsonObject1.getString("credit_amount");
                                accountHistory.reason=jsonObject1.getString("intent");
                                accountHistory.status=jsonObject1.getString("status");
                                accountHistory.create_time=jsonObject1.getString("create_time");
                                accountHistory.type=jsonObject1.getString("type");
                                accountHistoryList.add(accountHistory);

                            }


                        }
                        if(accountHistoryList.size()>0){

                            accountHistoryAdapter=new AccountHistoryAdapter(accountHistoryList);
                            recyclerView.setAdapter(accountHistoryAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            noDataText.setVisibility(View.GONE);

                        }
                        else {
                            recyclerView.setVisibility(View.GONE);
                            noDataText.setVisibility(View.VISIBLE);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

             }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
}
