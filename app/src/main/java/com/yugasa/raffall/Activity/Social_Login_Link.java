package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.yugasa.raffall.Constant;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramSession;
import net.londatiga.android.instagram.InstagramUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class Social_Login_Link extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    TextView skip, messageTxt, ok, toolbarTitle, skipIt, connect;
    ImageView backIcon, shareIcon;
    ProgressDialog progressDialog;
    Dialog dialog;
    String email, mobileNo;
    GoogleApiClient mGoogleApiClient;
    private GoogleApiClient client;
    List<String> permissions = new ArrayList<>();
    ImageView image1, image2, image3, image4;
    String id;
    Boolean flag1 = false, flag2 = false, flag3 = false, flag4 = false;
    Drawable fbIcon, instaIcon, twitterIcon, gmailIcon;
    private static final String TWITTER_KEY = "iv3sQM3iAKoprrneSu3I7EM1q";
    private static final String TWITTER_SECRET = "BLnu2jgddzqsn8hGPBtNBRPRsHnkSubhMiHwPjZuVLyUQWxWhI";
    private TwitterAuthClient twitterAuthClient;
    CallbackManager callbackManager;
    Intent intent;
    android.support.v7.widget.Toolbar toolbar;
    SharedPreferences sharedPreferences;
    String login_platform = "";

    Bitmap bmp1, bmp2, bmp3, bmp4, fbBitmap, twitterBitmap, instaBitmap, gmailBitmap;
    private static final String INSTA_KEY ="b207cda048a743179120f58f8d038101";
    private static final String INSTA_SECRET ="82b1029aacf64cb0843b17714ed4dd11";
    private static final String REDIRECT_URI ="http://isglobal.co/";
    private static final int RC_SIGN_IN = 9001;
    private InstagramSession mInstagramSession;
    private Instagram mInstagram;

    UserInfo userInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInstagram          = new Instagram(this, INSTA_KEY, INSTA_SECRET, REDIRECT_URI);
        mInstagramSession   = mInstagram.getSession();

        setContentView(R.layout.account_link_screen);

        FacebookSdk.sdkInitialize(this);


        image1 = (ImageView) findViewById(R.id.facebook);
        String backgroundImageName = String.valueOf(image1.getTag());
        image2 = (ImageView) findViewById(R.id.twitter);
        image3 = (ImageView) findViewById(R.id.insta);
        image4 = (ImageView) findViewById(R.id.gmail);
        backIcon = (ImageView) findViewById(R.id.back_icon);
        shareIcon = (ImageView) findViewById(R.id.share);
        skip = (TextView) findViewById(R.id.skip);
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);


        sharedPreferences = getSharedPreferences("App_Setting", MODE_PRIVATE);
        toolbarTitle.setText(getString(R.string.item_title2));
        shareIcon.setVisibility(View.GONE);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fbIcon = getResources().getDrawable(R.drawable.facebbok);
        twitterIcon = getResources().getDrawable(R.drawable.twitterr);
        instaIcon = getResources().getDrawable(R.drawable.instta);
        gmailIcon = getResources().getDrawable(R.drawable.ggmail);

        fbBitmap = ((BitmapDrawable) fbIcon).getBitmap();
        twitterBitmap = ((BitmapDrawable) twitterIcon).getBitmap();
        instaBitmap = ((BitmapDrawable) instaIcon).getBitmap();
        gmailBitmap = ((BitmapDrawable) gmailIcon).getBitmap();

        intent=getIntent();
        if(intent!=null){

            if(intent.getIntExtra("type",-1)==1){
                skip.setVisibility(View.GONE);

            }
            else if(intent.getIntExtra("type",-1)==0){

                toolbar.setVisibility(View.GONE);
            }
        }



       /* if (sharedPreferences.getBoolean("isShowCheckToolbar", false) == true) {

            toolbar.setVisibility(View.GONE);

        }
*/
        userInfo = DroidPrefs.get(Social_Login_Link.this, "user_info", UserInfo.class);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        twitterAuthClient = new TwitterAuthClient();


        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag2 = true;
                if (bmp2.sameAs(twitterBitmap)) {
                    if (userInfo.twitterId!=null && !userInfo.twitterId.equals("") && !userInfo.twitterId.equalsIgnoreCase("null"))
                    {

                    }else {
                        twitterLogin();
                    }
                } else if (bmp2.sameAs(fbBitmap)) {
                    if (userInfo.facebookId!=null && !userInfo.facebookId.equals("") && !userInfo.facebookId.equalsIgnoreCase("null"))
                    {

                    }else {
                        fblogin();
                    }
                } else if (bmp2.sameAs(gmailBitmap)) {
                    if (userInfo.gmailId!=null && !userInfo.gmailId.equals("") && !userInfo.gmailId.equalsIgnoreCase("null"))
                    {

                    }else {
                        gooleSignIn();
                    }
                }
                else if (bmp2.sameAs(instaBitmap)) {
                    if (userInfo.instaId!=null && !userInfo.instaId.equals("") && !userInfo.instaId.equalsIgnoreCase("null"))
                    {

                    }else {
                        instaLogin();
                    }
                }

            }
        });

        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag3 = true;
                if (bmp3.sameAs(twitterBitmap)) {
                    if (userInfo.twitterId!=null && !userInfo.twitterId.equals("") && !userInfo.twitterId.equalsIgnoreCase("null"))
                    {

                    }else {
                        twitterLogin();
                    }
                } else if (bmp3.sameAs(fbBitmap)) {
                    if (userInfo.facebookId!=null && !userInfo.facebookId.equals("") && !userInfo.facebookId.equalsIgnoreCase("null"))
                    {

                    }else {
                        fblogin();
                    }
                } else if (bmp3.sameAs(gmailBitmap)) {
                    if (userInfo.gmailId!=null && !userInfo.gmailId.equals("") && !userInfo.gmailId.equalsIgnoreCase("null"))
                    {

                    }else {
                        gooleSignIn();
                    }
                }
                else if (bmp3.sameAs(instaBitmap)) {
                    if (userInfo.instaId!=null && !userInfo.instaId.equals("") && !userInfo.instaId.equalsIgnoreCase("null"))
                    {

                    }else {
                        instaLogin();
                    }
                }

            }
        });

        image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag4 = true;
                if (bmp4.sameAs(twitterBitmap)) {
                    if (userInfo.twitterId!=null && !userInfo.twitterId.equals("") && !userInfo.twitterId.equalsIgnoreCase("null"))
                    {

                    }else {
                        twitterLogin();
                    }
                } else if (bmp4.sameAs(fbBitmap)) {
                    if (userInfo.facebookId!=null && !userInfo.facebookId.equals("") && !userInfo.facebookId.equalsIgnoreCase("null"))
                    {

                    }else {
                        fblogin();
                    }

                } else if (bmp4.sameAs(gmailBitmap)) {
                    if (userInfo.gmailId!=null && !userInfo.gmailId.equals("") && !userInfo.gmailId.equalsIgnoreCase("null"))
                    {

                    }else {
                        gooleSignIn();
                    }
                }
                else if (bmp4.sameAs(instaBitmap)) {
                    if (userInfo.instaId!=null && !userInfo.instaId.equals("") && !userInfo.instaId.equalsIgnoreCase("null"))
                    {

                    }else {
                        instaLogin();
                    }
                }

            }
        });





//fb link.............................................

        callbackManager = CallbackManager.Factory.create();


        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                String userId = accessToken.getUserId();

                getFacebookData(accessToken);
                //Toast.makeText(Splash_Activity.this, "successful", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancel() {
                Log.e("error", "error");
            }

            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                        Log.e("ggg", "error" + error);
                    }
                }
            }
        });


        //Google link//.............................
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showdialog();
            }
        });


        if (userInfo.loginPlatform.equalsIgnoreCase("twitter")) {
            image1.setImageResource(R.drawable.twitterrclick);
            image2.setImageResource(R.drawable.facebbok);
            image3.setImageResource(R.drawable.instta);
            image4.setImageResource(R.drawable.ggmail);
        } else if (userInfo.loginPlatform.equalsIgnoreCase("facebook")) {
            image1.setImageResource(R.drawable.facebookclick);
            image2.setImageResource(R.drawable.twitterr);
            image3.setImageResource(R.drawable.instta);
            image4.setImageResource(R.drawable.ggmail);

        } else if (userInfo.loginPlatform.equalsIgnoreCase("instagram")) {
            image1.setImageResource(R.drawable.insttaclick);
            image2.setImageResource(R.drawable.facebbok);
            image3.setImageResource(R.drawable.twitterr);
            image4.setImageResource(R.drawable.ggmail);

        } else if (userInfo.loginPlatform.equalsIgnoreCase("gmail")) {
            image1.setImageResource(R.drawable.ggmailclick);
            image2.setImageResource(R.drawable.facebbok);
            image3.setImageResource(R.drawable.twitterr);
            image4.setImageResource(R.drawable.instta);

        }
        bmp1 = ((BitmapDrawable) image1.getDrawable()).getBitmap();
        bmp2 = ((BitmapDrawable) image2.getDrawable()).getBitmap();
        bmp3 = ((BitmapDrawable) image3.getDrawable()).getBitmap();
        bmp4 = ((BitmapDrawable) image4.getDrawable()).getBitmap();

         CheckimageBlur();

    }

    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        TwitterSession session = result.data;

        //Getting the username from session
        final String username = session.getUserName();

        //This code will fetch the profile image URL
        //Getting the account service of the user logged in

        Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {
                    @Override
                    public void failure(TwitterException e) {
                        //If any error occurs handle it here
                        Toast.makeText(Social_Login_Link.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void success(Result<User> userResult) {
                        //If it succeeds creating a User object from userResult.data
                        User user = userResult.data;

                        //Getting the profile image url


                        id = user.idStr;
                        login_platform = "twitter";
                        email = user.email;
                        mobileNo = "";
                        regis(login_platform, id, email, mobileNo);


                    }
                });
    }


    private void regis(String login_platform, String id, String email, String mobNo) {
        if (Constant.isInternetConnected(this)) {
            String url = APIConstantsDemo.OTHER_SOCIAL_LOGIN_URL;
            Log.i("url", url);
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Registering...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            UserInfo userinfo = DroidPrefs.get(this, "user_info", UserInfo.class);


            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("user_token",userInfo.userToken);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObj.put("user_id", userinfo.userId);
                if (login_platform.equalsIgnoreCase("facebook")) {
                    jsonObj.put("social_id", id);
                }


                if (login_platform.equalsIgnoreCase("twitter")) {
                    jsonObj.put("social_id", id);
                }

                if (login_platform.equalsIgnoreCase("gmail")) {
                    jsonObj.put("social_id", id);
                }

                if (login_platform.equalsIgnoreCase("instagram")) {
                    jsonObj.put("social_id", id);
                }


                jsonObj.put("social_media_name", login_platform);
                if (email!=null && email.equalsIgnoreCase("null")) {
                    jsonObj.put("email", email);
                }
                else{
                    jsonObj.put("email", "");
                }
                if (mobNo!=null && mobNo.equalsIgnoreCase("null")) {
                    jsonObj.put("mobile_no", mobNo);
                }else{
                    jsonObj.put("mobile_no", "");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Log.d("checkvalue",""+id+" "+userinfo.userId+" "+login_platform);
            Log.d("checkvalue", "" + jsonObj);
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        progressDialog.dismiss();
                        try {
                            if(response.getString("status").equalsIgnoreCase("success")){
                                JSONObject jsonobj = response.getJSONObject("data");
                                if (jsonobj != null && jsonobj.length() > 0) {
                                    //JSONObject jsonObject1 = jsonobj.getJSONObject(0);
                                    UserInfo user = new UserInfo();


                                    user.name = jsonobj.getString("name");
                                    user.email = jsonobj.getString("email");
                                    user.profile_image = jsonobj.getString("image");
                                    user.mobileNo = jsonobj.getString("mobile_no");
                                    user.loginPlatform = jsonobj.getString("login_platform");
                                    user.address=jsonobj.getString("address");
                                    user.facebookId=jsonobj.getString("facebook_id");
                                    user.twitterId=jsonobj.getString("twitter_id");
                                    user.gmailId=jsonobj.getString("gmail_id");
                                    user.instaId=jsonobj.getString("instagram_id");
                                    user.userId=jsonobj.getString("user_id");
                                    user.deviceType=jsonobj.getString("device_type");
                                    user.deviceId=jsonobj.getString("device_id");
                                    user.userToken=jsonobj.getString("user_token");


                                    DroidPrefs.apply(Social_Login_Link.this,"user_info",user);
//

                                   // Toast.makeText(Social_Login_Link.this, user.name, Toast.LENGTH_SHORT).show();
//                                    userInfoList.add(user);
                                    //  Toast.makeText(Splash_Activity.this, "Successfully login. Welcome to Raffall.", Toast.LENGTH_SHORT).show();

                                }
                                showdialog(response.getString("message"));
                            }
                            else if(response.getString("status").equalsIgnoreCase("failed")){
                                Toast.makeText(Social_Login_Link.this, ""+response.getString("message"), Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Log.d("volleyerrot", "" + error);
                }
            });
            MySingleton.getInstance(this).addToRequestQueue(jsonRequest);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                    DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));
            //////////////////////////////////////////////////////////////////////////////

        }


    }


    public void fblogin() {
        if (Constant.isInternetConnected(this)) {
            permissions.add("email");
            permissions.add("public_profile");
            LoginManager.getInstance().logInWithReadPermissions(this, permissions);
        } else {
            Toast.makeText(Social_Login_Link.this, "This feature requires internet connection please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFacebookData(AccessToken accessToken) {
        if (Constant.isInternetConnected(this)) {
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(final JSONObject object, GraphResponse response) {
                            System.out.println("object = " + object);
//                            Log.e("myname", object.toString());
                            try {
                                id = object.getString("id");
                                login_platform = "facebook";
                                email = object.getString("email");
                              //  mobileNo = object.getString("mobile_no");

                                regis(login_platform, id, email, mobileNo);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,age_range");
            request.setParameters(parameters);
            request.executeAsync();

        } else {
            Toast.makeText(this, "This feature requires internet connection  please check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);


        }




    }


    private Instagram.InstagramAuthListener mAuthListener = new Instagram.InstagramAuthListener() {
        @Override
        public void onSuccess(InstagramUser user) {
            //showToast(user.fullName);
              id=user.id;
              login_platform="instagram";
              mobileNo="";
              email="";


            regis(login_platform,id,email,mobileNo);
        }

        @Override
        public void onError(String error) {
           // showToast(error);
            Log.d("error",""+error);

        }

        @Override
        public void onCancel() {
           // showToast("cancel");


        }
    };
    public void instaLogin(){
        Log.d("","");
        mInstagram.authorize(mAuthListener);
    }


    public void twitterLogin() {
        boolean isAppInstalled = appInstalledOrNot("com.twitter.android");
        if (isAppInstalled) {
            twitterAuthClient.authorize(Social_Login_Link.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> twitterSessionResult) {
                    // Success
                    login(twitterSessionResult);
                }

                @Override
                public void failure(TwitterException e) {
                    e.printStackTrace();
                }
            });

        }
        else {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.twitter.android"));
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("signingoogle", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
           // Toast.makeText(Social_Login_Link.this, "Success", Toast.LENGTH_SHORT).show();
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();


            login_platform = "gmail";

            id = acct.getId();
            email = acct.getEmail();
            mobileNo = "";
            regis(login_platform, id, email, mobileNo);


        }
    }

    private void gooleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    public void showdialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Social_Login_Link.this);
        View view = getLayoutInflater().inflate(R.layout.friend_refferal_dialog, null);
        builder.setView(view);

        messageTxt = (TextView) view.findViewById(R.id.text);
        ok = (Button) view.findViewById(R.id.buttonOk);
        messageTxt.setText(message);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckimageBlur();
                skip.setText(R.string.next);
                skip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(Social_Login_Link.this,MainActivity.class);
                        startActivity(intent);
                    }
                });

                /*if (flag2) {
                    flag2 = false;
                    image2.setClickable(false);
                    image2.setAlpha(0.5f);

                    skip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(Social_Login_Link.this,MainActivity.class);
                            startActivity(intent);
                        }
                    });

                } else if (flag3) {
                    flag3 = false;
                    image3.setClickable(false);
                    image3.setAlpha(0.5f);
                    skip.setText(R.string.next);
                    skip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(Social_Login_Link.this,MainActivity.class);
                            startActivity(intent);
                        }
                    });


                } else if (flag4) {
                    flag4 = false;
                    image4.setClickable(false);
                    image4.setAlpha(0.5f);
                    skip.setText(R.string.next);
                    skip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(Social_Login_Link.this,MainActivity.class);
                            startActivity(intent);
                        }
                    });


                } else {
                    dialog.dismiss();
                }*/
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }

    public void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Social_Login_Link.this);
        View view = getLayoutInflater().inflate(R.layout.link_skip_dialog, null);
        builder.setView(view);


        skipIt = (TextView) view.findViewById(R.id.skipIt);
        connect = (TextView) view.findViewById(R.id.letUsConnnet);


        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        skipIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Social_Login_Link.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        dialog = builder.create();

        dialog.show();


    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
         public void CheckimageBlur(){
              userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);
             if (userInfo.facebookId!=null && !userInfo.facebookId.equalsIgnoreCase("null") && !userInfo.facebookId.equals("")){
                 if (bmp1.sameAs(fbBitmap)) {
                     image1.setAlpha(0.5f);
                 } else if (bmp2.sameAs(fbBitmap)) {
                     image2.setAlpha(0.5f);
                 } else if (bmp3.sameAs(fbBitmap)) {
                     image3.setAlpha(0.5f);
                 }
                 else if (bmp4.sameAs(fbBitmap)) {
                     image4.setAlpha(0.5f);
                 }
                 else{

                 }
             }
             if (userInfo.gmailId!=null && !userInfo.gmailId.equalsIgnoreCase("null") && !userInfo.gmailId.equals("")){
                 if (bmp1.sameAs(gmailBitmap)) {
                     image1.setAlpha(0.5f);
                 } else if (bmp2.sameAs(gmailBitmap)) {
                     image2.setAlpha(0.5f);
                 } else if (bmp3.sameAs(gmailBitmap)) {
                     image3.setAlpha(0.5f);
                 }
                 else if (bmp4.sameAs(gmailBitmap)) {
                     image4.setAlpha(0.5f);
                 }
                 else{

                 }
             }
             if (userInfo.twitterId!=null && !userInfo.twitterId.equalsIgnoreCase("null") && !userInfo.twitterId.equals("")){
                 if (bmp1.sameAs(twitterBitmap)) {
                     image1.setAlpha(0.5f);
                 } else if (bmp2.sameAs(twitterBitmap)) {
                     image2.setAlpha(0.5f);
                 } else if (bmp3.sameAs(twitterBitmap)) {
                     image3.setAlpha(0.5f);
                 }
                 else if (bmp4.sameAs(twitterBitmap)) {
                     image4.setAlpha(0.5f);
                 }
                 else{

                 }
             }
             if (userInfo.instaId!=null && !userInfo.instaId.equalsIgnoreCase("null") && !userInfo.instaId.equals("")){
                 if (bmp1.sameAs(instaBitmap)) {
                     image1.setAlpha(0.5f);
                 } else if (bmp2.sameAs(instaBitmap)) {
                     image2.setAlpha(0.5f);
                 } else if (bmp3.sameAs(instaBitmap)) {
                     image3.setAlpha(0.5f);
                 }
                 else if (bmp4.sameAs(instaBitmap)) {
                     image4.setAlpha(0.5f);
                 }
                 else{

                 }
             }

         }
}