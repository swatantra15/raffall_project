package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RafferalCode;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.Items.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GetFreeCreditActivity extends AppCompatActivity {
    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.refferalCode)
    TextView myrefferalCode;
    @BindView(R.id.back_icon)
    ImageView backIcon;
    @BindView(R.id.credit_txt1)
    TextView creditText1;
    @BindView(R.id.facebook)
    ImageView facebookIcon;
    @BindView(R.id.twitter)
    ImageView twitterIcon;
    @BindView(R.id.global_share)
    ImageView globalShare;
    @BindView(R.id.credit_text2)
    TextView creditText2;
@BindView(R.id.share)
        ImageView shareIcon;
    ListView listView;
    String[] socialsites={"Facebook","Twitter","WhatsApp","Gmail","Copy"};
    ArrayAdapter shareAdapter;
    ProgressDialog progressDialog;
    List<String> shareitems;
    List<RafferalCode> codeList;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    String urlFb="https://www.facebook.com/RafflTicketApp/";
    String urlFb1="\"<a href='https://www.facebook.com/RafflTicketApp/'>www.facebook.com/RafflTicketApp</a>\";";
    String urlTwitter= "https://twitter.com/RafflTicketApp";
    String urlGlobal= "https://www.rafflticket.com";
    String link;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_free_credit);
        ButterKnife.bind(this);
         toolbarTitle.setText(R.string.item_title6);
        codeList=new ArrayList<>();
        getRafferalCode();

       // Toast.makeText(GetFreeCreditActivity.this, "sheh", Toast.LENGTH_SHORT).show();

        //back button functionality
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        facebookIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlFb));
                startActivity(intent);
            }
        });

        twitterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlTwitter));
                startActivity(intent);
            }
        });

        globalShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlGlobal));
                startActivity(intent);
            }
        });
        shareIcon.setVisibility(View.GONE);
    }


    public void getRafferalCode(){
        String url= APIConstantsDemo.REFFERAL_URL;
        Log.i("url",url);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){

                            JSONObject jsonObj=response.getJSONObject("data");

                            RafferalCode refferalCode=new RafferalCode();

                            refferalCode.myRafferalCode=jsonObj.getString("referral_code");
                            refferalCode.friendRafferalCode=jsonObj.getString("friend_referral_code");
                            refferalCode.pointsPerShare=jsonObj.getString("points_per_share");





                            //Toast.makeText(GetFreeCreditActivity.this, "listsize "+codeList.size(), Toast.LENGTH_SHORT).show();

                myrefferalCode.setText(refferalCode.myRafferalCode);
               // creditText1.setText("and you\"ll recieve £ "+refferalCode.pointsPerShare+" credit for each one that does.");
                creditText2.setText("and you\"ll recieve £ "+refferalCode.pointsPerShare+" credit for each one that does.");



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

        myrefferalCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              // showDialog();
                sendData(Constant.SHARE_TEXT);
                //Constant constant=new Constant();
               // constant.ShowDialog(GetFreeCreditActivity.this,myrefferalCode.getText().toString(),null,GetFreeCreditActivity.this,"");
            }
        });


    }
    public void showDialog() {
        bottomSheetDialog = new BottomSheetDialog(this);
        View v = getLayoutInflater().inflate(R.layout.share_bottomsheet_dialog, null);
        bottomSheetDialog.setContentView(v);
        bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());
        listView=v.findViewById(R.id.listItem);




        shareAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.share_menu));
        listView.setAdapter(shareAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                   // sendData("com.twitter.android","check/"+myrefferalCode.getText().toString());
                }
                else if(position==1){

                }
                else if(position==2){

                }
                else if(position==3){

                }
                else if(position==4){

                }
                else if(position==5){

                }
            }
        });
      // shareitems.add(getResources().getStringArray(R.array.share_menu));

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.show();


    }
       public void sendData(String message){

           /*Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
           shareIntent.setType("text/plain");
           shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);

            shareIntent = getPackageManager().getLaunchIntentForPackage(application);
                   shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                   shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

                        startActivity(shareIntent);
*/
           String shareBody = "https://play.google.com/store/apps/details?id=com.twitter.android";
           link=message+getString(R.string.readyandroid);

           Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
           sharingIntent.setType("text/plain");
         //  sharingIntent = getPackageManager().getLaunchIntentForPackage(application);
         //  sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");

           sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,message+" : "+myrefferalCode.getText().toString()+"\n"+" "+shareBody+"\n");
           startActivity(Intent.createChooser(sharingIntent, "Share URl"));


       }
          /* Intent intent = getPackageManager().getLaunchIntentForPackage(application);
           if (intent != null) {
               // The application exists
               Intent shareIntent = new Intent();
               shareIntent.setAction(Intent.ACTION_SEND);
               shareIntent.setType("type/plain");
               shareIntent.setPackage(application);

               shareIntent.putExtra(Intent.EXTRA_TEXT, message);
               // Start the specific social application
               startActivity(shareIntent);
           } else {
               // The application does not exist
               // Open GooglePlay or use the default system picker
           }*/
       }


