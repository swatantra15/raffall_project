package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.NotificationData;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class Winner_Review extends AppCompatActivity {
    EditText editTextView;
    ProgressDialog progressDialog;
    TextView productName, submit, skip, ok,txtheader,txtBody;
    ImageView productImage;
    RatingBar ratingBar;
    Dialog dialog;

    Float f=5.0f;
    CircleImageView userImage;
    ImageLoader imageLoader;
    NotificationData notificationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.winner_rating);

        productName = (TextView) findViewById(R.id.raffalProductName);
        submit = (TextView) findViewById(R.id.submit);
        skip = (TextView) findViewById(R.id.skip);
        productImage = (ImageView) findViewById(R.id.imageviewItem);
        userImage = (CircleImageView) findViewById(R.id.user_image);
        ratingBar = (RatingBar) findViewById(R.id.rating);

        imageLoader = MySingleton.getInstance(this).getImageLoader();

        Intent intent = getIntent();
        if (intent != null) {
            notificationData = (NotificationData) intent.getSerializableExtra("data1");

        }
        editTextView = (EditText) findViewById(R.id.editTextReview);
        hideSoftKeyboard(editTextView);

        editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
editTextView.setFocusableInTouchMode(true);
                showSoftKeyboard(editTextView);

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitReview();
                finish();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(Winner_Review.this,MainActivity.class);
                startActivity(intent1);
                finish();
            }
        });

        productName.setText(notificationData.productName);
       // hideSoftKeyboard();
        if (notificationData.productImage != null) {
            imageLoader.get(notificationData.productImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        productImage.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
        if (notificationData.userImage != null) {
            imageLoader.get(notificationData.userImage, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {

                        userImage.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (fromUser) {
                    f = rating;

                }
            }
        });
    }

    public void hideSoftKeyboard(View view) {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view
                    .getWindowToken(), 0);
        }
    }


    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view,InputMethodManager.SHOW_IMPLICIT);
    }

    public void submitReview() {
        String url = APIConstantsDemo.SUBMIT_REVIEW;
        Log.i("url", url);
        UserInfo userInfo = DroidPrefs.get(this, "user_info", UserInfo.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("winner_id", notificationData.winnerId);
            jsonObject.put("user_token", userInfo.userToken);
            jsonObject.put("review", editTextView.getText().toString());
            jsonObject.put("rating", f);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

showdialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

    public void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Winner_Review.this);
        View view = getLayoutInflater().inflate(R.layout.ticket_confirm_dialog, null);
        builder.setView(view);


        ok = (TextView) view.findViewById(R.id.ok);
        txtheader = (TextView) view.findViewById(R.id.textHeadline);
        txtBody = (TextView) view.findViewById(R.id.txtForNoConnection);

        txtheader.setText("Successful");
        txtBody.setText(" Thank you, You have successfully submitted your review.");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               Intent intent =new Intent(Winner_Review.this,MainActivity.class);
               startActivity(intent);
               finish();

            }
        });
        dialog = builder.create();

        dialog.show();

    }
}