package com.yugasa.raffall.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.crashlytics.android.Crashlytics;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import io.fabric.sdk.android.Fabric;


/**
 * Created by yugasalabs-16 on 20/9/17.
 */

public class SplashScreen extends AppCompatActivity {

    UserInfo userInfo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this,  new CrashlyticsNdk());

       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.layout_splash);


      //  Log.i("ggd",null);
       userInfo= DroidPrefs.get(SplashScreen.this,"user_info",UserInfo.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(userInfo!=null && userInfo.userId!=null){
                    startActivity(new Intent(SplashScreen.this,MainActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashScreen.this,Splash_Activity.class));
                    finish();
                }
            }
        },2000);
    }
}
