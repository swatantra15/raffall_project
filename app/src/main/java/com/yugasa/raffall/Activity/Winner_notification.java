package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.NotificationData;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class Winner_notification extends AppCompatActivity {
    TextView proceed,userName,productName;
    NotificationData  notificationData;
ImageLoader imageLoader;
ProgressDialog progressDialog;
String url;
ImageView userImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.winner_notification);

        proceed=(TextView)findViewById(R.id.proceed);
        userName=(TextView)findViewById(R.id.userName);
        productName=(TextView)findViewById(R.id.product);
        userImage=(ImageView) findViewById(R.id.user_image);

          imageLoader= MySingleton.getInstance(this).getImageLoader();

        Intent intent=getIntent();
        if (intent!=null){
            if(intent.getIntExtra("type",-1)==1){
                notificationData= (NotificationData) intent.getSerializableExtra("data");
                badgeCount();

            }

        }


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Winner_notification.this,Winner_Review.class);
                intent.putExtra("data1",notificationData);
                startActivity(intent);
            }
        });

      userName.setText(notificationData.userName);
      productName.setText("You won successfully "+notificationData.productName);
          if(notificationData.userImage !=null){

    Picasso.with(this).load(notificationData.userImage).into(userImage);
        /*imageLoader.get(notificationData.userImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    userImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });*/
    }}
    public void badgeCount() {
        String url = APIConstantsDemo.BADGE_COUNT;
        Log.i("url", url);
        UserInfo userInfo = DroidPrefs.get(this, "user_info", UserInfo.class);
       /* progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userInfo.userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               // progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                       // progressDialog.dismiss();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
}
