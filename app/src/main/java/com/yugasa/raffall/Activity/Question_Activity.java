package com.yugasa.raffall.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.twitter.sdk.android.core.internal.TwitterCollection;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.PuzzleAnswer;
import com.yugasa.raffall.PuzzleOption;
import com.yugasa.raffall.QuestionPuzzle;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RafflPuzzleQuestion;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Question_Activity extends AppCompatActivity {
    ProgressDialog progressDialog;
    Intent intent;
    TextView dialogTitle,message,buttonYes,buttonNo,toolbarTitle;
    float total_amount,ticketPriceInt,noOfTicketInt,tic;
    TextView quesTxt1,quesTxt2,optionTxt1,optionTxt2,optionTxt3,optionTxt4,noTickets,ok,cancel,confirm,txt1,txt2,txt3;
    ImageView optImg1,optImg2,optImg3,optImg4,quesImg,increment,decrement,shareIcon,backIcon;
    String product_id,avialableTicket,noOfTickets,ticketPrice,totalTicket;
    int present_value_int;
    Button play;
    String optionFormat;
    boolean IS_CHECK_SUCCESS=false;
    String optValue;
    Dialog dialog;
    String present_value_string;
    LinearLayout linearTxt,linearImg;
    QuestionPuzzle questionPuzzle;
    String checkAnswerStaus="";
    TextView txt_ratio;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_);

        quesImg=(ImageView)findViewById(R.id.quesImage);
        shareIcon=(ImageView)findViewById(R.id.share);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        optImg1=(ImageView)findViewById(R.id.optImg1);
        optImg2=(ImageView)findViewById(R.id.optImg2);
        optImg3=(ImageView)findViewById(R.id.optImg3);
        optImg4=(ImageView)findViewById(R.id.optImg4);
        quesTxt1=(TextView)findViewById(R.id.questionTxt1);
        quesTxt2=(TextView)findViewById(R.id.quesTxt2);
        optionTxt1=(TextView)findViewById(R.id.optxt1);
        optionTxt2=(TextView)findViewById(R.id.optxt2);
        optionTxt3=(TextView)findViewById(R.id.optxt3);
        optionTxt4=(TextView)findViewById(R.id.optxt4);
        noTickets=(TextView)findViewById(R.id.no_of_ticket);
        toolbarTitle=(TextView)findViewById(R.id.toolbarTitle);
        increment=(ImageView)findViewById(R.id.increment);
        decrement=(ImageView) findViewById(R.id.decrement);
        linearTxt=(LinearLayout) findViewById(R.id.lineartxt);
        linearImg=(LinearLayout) findViewById(R.id.linearImage);
        play=(Button) findViewById(R.id.play);
        txt_ratio=findViewById(R.id.txt_ratio);

toolbarTitle.setText("Question");

shareIcon.setVisibility(View.GONE);
backIcon.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});

    //final int totalTicket=Integer.parseInt(questionPuzzle.totalToken);

        intent=getIntent();
        if(intent!=null){

            product_id=intent.getStringExtra("productID");
            avialableTicket=intent.getStringExtra("freeTicket");
            totalTicket=intent.getStringExtra("totalTicket");
            final int total_token=Integer.parseInt(totalTicket);

            getRafflPuzzle(product_id);

optionTxt1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        optionTxt1.setBackground(getResources().getDrawable(R.color.app_background));
        optionTxt2.setBackground(getResources().getDrawable(R.color.txt_back));
        optionTxt3.setBackground(getResources().getDrawable(R.color.txt_back));
        optionTxt4.setBackground(getResources().getDrawable(R.color.txt_back));
        optValue= optionTxt1.getText().toString().trim();
    }
});

            optionTxt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionTxt2.setBackground(getResources().getDrawable(R.color.app_background));
                    optionTxt1.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt3.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt4.setBackground(getResources().getDrawable(R.color.txt_back));
                     optValue= optionTxt2.getText().toString();
                }
            });

            optionTxt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionTxt3.setBackground(getResources().getDrawable(R.color.app_background));
                    optionTxt1.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt2.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt4.setBackground(getResources().getDrawable(R.color.txt_back));
                     optValue= optionTxt3.getText().toString();
                }
            });

            optionTxt4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionTxt4.setBackground(getResources().getDrawable(R.color.app_background));
                    optionTxt1.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt2.setBackground(getResources().getDrawable(R.color.txt_back));
                    optionTxt3.setBackground(getResources().getDrawable(R.color.txt_back));
                    optValue= optionTxt4.getText().toString();
                }
            });
            increment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt(noTickets.getText().toString())<Integer.parseInt(avialableTicket)) {
                        String present_value_string = noTickets.getText().toString();
                        int present_value_int = Integer.parseInt(present_value_string);
                        present_value_int++;


                        noTickets.setText(String.valueOf(present_value_int));
                        String diff;
                        float d=(float)present_value_int/(float)total_token;
                        if(d==0){
                            diff=present_value_int+"/"+total_token;
                        }
                        else {
                      diff =toFraction(d, total_token);

                        }

                        Log.i("diff",diff);




                          String text="<b>By entering this competition you will have a minimum </b><font color=#a9d267><b>"+ diff+"</b></font><b> chance of winning this </b>"+questionPuzzle.productName;
                        txt_ratio.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                    }
                }
            });

            optImg1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optImg1.setBackground(getResources().getDrawable(R.color.app_background));
                    optImg2.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg3.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg4.setBackground(getResources().getDrawable(R.color.txt_dark_back));

                    for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
                        if (i==0){
                            optValue=questionPuzzle.rafflPuzzleOption.get(i).optionValue;
                            break;
                        }
                    }
                }
            });

            optImg2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optImg2.setBackground(getResources().getDrawable(R.color.app_background));
                    optImg1.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg3.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg4.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
                        if (i==1){
                            optValue=questionPuzzle.rafflPuzzleOption.get(i).optionValue;
                            break;
                        }
                    }

                   // optValue= optImg2.
                }
            });

            optImg3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optImg3.setBackground(getResources().getDrawable(R.color.app_background));
                    optImg1.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg2.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg4.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
                        if (i==2){
                            optValue=questionPuzzle.rafflPuzzleOption.get(i).optionValue;
                            break;
                        }
                    }
                }
            });

            optImg4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optImg4.setBackground(getResources().getDrawable(R.color.app_background));
                    optImg1.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg2.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    optImg3.setBackground(getResources().getDrawable(R.color.txt_dark_back));
                    for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
                        if (i==3){
                            optValue=questionPuzzle.rafflPuzzleOption.get(i).optionValue;

                            break;
                        }
                    }
                }
            });

            decrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Integer.parseInt(noTickets.getText().toString())>1){
                         present_value_string = noTickets.getText().toString();
                         present_value_int = Integer.parseInt(present_value_string);
                        present_value_int--;
                        noTickets.setText(String.valueOf(present_value_int));
                        String diff;
                        float d=(float)present_value_int/(float)total_token;
                        if(d==0){
                            diff=present_value_int+"/"+totalTicket;
                        }
                        else {
                            diff =toFraction(d, total_token);
                        }

                        String text="By entering this competition you will have a minimum <font color=#a9d267><b>"+ diff+"</b></font> chance of winning this "+questionPuzzle.productName;
                        txt_ratio.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                    }



                }
            });

            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (optionFormat.equalsIgnoreCase("0")) {
                        if(questionPuzzle.rafflPuzzleQuestion.quesImage.equals("") && questionPuzzle.rafflPuzzleQuestion.quesImage.equalsIgnoreCase("null")){


                        }
                        else {
                            Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleQuestion.quesImage).into(quesImg);
                        }
                        if (optionTxt1.getText().toString().equalsIgnoreCase(optValue)) {
                            showdialog1(noTickets.getText().toString(),optValue,questionPuzzle.tokenPrice,questionPuzzle.productName, 0);
                        } else if (optionTxt2.getText().toString().equals(optValue)) {
                            showdialog1(noTickets.getText().toString(),optValue,questionPuzzle.tokenPrice,questionPuzzle.productName, 0);
                        } else if (optionTxt3.getText().toString().equals(optValue)) {
                            showdialog1(noTickets.getText().toString(),optValue,questionPuzzle.tokenPrice,questionPuzzle.productName, 0);
                        } else if (optionTxt4.getText().toString().equals(optValue)) {
                            showdialog1(noTickets.getText().toString(),optValue,questionPuzzle.tokenPrice,questionPuzzle.productName,0);
                        } else {
                            showdialog();
                        }

                    }
                    else if(optionFormat.equalsIgnoreCase("1")) {
                        boolean no_selected=false;
                        int position=-1;
                        if(questionPuzzle.rafflPuzzleQuestion.quesImage.equals("") && questionPuzzle.rafflPuzzleQuestion.quesImage.equalsIgnoreCase("null")){


                        }
                        else {
                            Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleQuestion.quesImage).into(quesImg);
                        }
                        for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
position=i;

                            if (questionPuzzle.rafflPuzzleOption.get(i).optionValue.equalsIgnoreCase(optValue)){
no_selected=true;

                            }

                        }
                        if(no_selected){
                            showdialog1(noTickets.getText().toString(), optValue, questionPuzzle.tokenPrice, questionPuzzle.productName,position+1);
                        }
                        else{
                            showdialog();
                        }
                    }
                        else {
                            showdialog();
                        }
                    }

//                    if(optImg1.getText().toString().equals(optValue)) {
//
//                        showdialog1();
//                }}
                 });


    }}
    public void getRafflPuzzle(String product_id){
        String url=APIConstantsDemo.QUESURL;
        Log.i("url", url);
        UserInfo userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("raffle_product_id",product_id);
            jsonObject.put("user_token",userInfo.userToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){

                            JSONObject jsonObj=response.getJSONObject("data");

                             questionPuzzle=new QuestionPuzzle();

                             questionPuzzle.productName=jsonObj.getString("raffle_product_name");
                            questionPuzzle.totalToken=jsonObj.getString("total_token");
                            questionPuzzle.tokenPrice=jsonObj.getString("token_price");
                            questionPuzzle.availableToken=jsonObj.getString("available_token");
                            questionPuzzle.optionFormat=jsonObj.getString("options_format");
                                 optionFormat=questionPuzzle.optionFormat;
                            questionPuzzle.rafflPuzzleQuestion=new RafflPuzzleQuestion();
                            JSONObject jsonObj2=jsonObj.getJSONObject("raffle_puzzle_question");
                            questionPuzzle.rafflPuzzleQuestion.id=jsonObj2.getString("id");
                            questionPuzzle.rafflPuzzleQuestion.productId=jsonObj2.getString("raffle_product_id");
                            questionPuzzle.rafflPuzzleQuestion.quesImage=jsonObj2.getString("question_image");
                            questionPuzzle.rafflPuzzleQuestion.quesText=jsonObj2.getString("question_text");
                            questionPuzzle.rafflPuzzleQuestion.status=jsonObj2.getString("status");

                            questionPuzzle.puzzleAnswer=new PuzzleAnswer();
                            JSONObject jsonObj1=jsonObj.getJSONObject("raffle_puzzle_answer");
                            questionPuzzle.puzzleAnswer.id=jsonObj1.getString("id");
                            questionPuzzle.puzzleAnswer.quesId=jsonObj1.getString("raffle_puzzle_question_id");
                            questionPuzzle.puzzleAnswer.answer=jsonObj1.getString("answer");


                            JSONArray jsonArray=jsonObj.optJSONArray("raffle_puzzle_options");
                            questionPuzzle.rafflPuzzleOption=new ArrayList<>();
                            if(jsonArray!=null && jsonArray.length()>0){

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    PuzzleOption puzzleOption=new PuzzleOption();
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                     puzzleOption.id=jsonObject1.getString("id");
                                     puzzleOption.optionFormat=jsonObject1.getString("option_format");
                                     puzzleOption.optionValue=jsonObject1.getString("option_value");
                                     puzzleOption.questionId=jsonObject1.getString("raffle_puzzle_question_id");


                                   questionPuzzle.rafflPuzzleOption.add(puzzleOption);

                                }
                            }

                            if(questionPuzzle.optionFormat.equals("0")){
                                linearImg.setVisibility(View.GONE);
                                linearTxt.setVisibility(View.VISIBLE);
                                quesTxt1.setText(questionPuzzle.rafflPuzzleQuestion.quesText);
                                if (questionPuzzle.rafflPuzzleQuestion.quesImage.equals("") || questionPuzzle.rafflPuzzleQuestion.quesImage.equals("null")){
                                    quesImg.setVisibility(View.GONE);

                                }else {
                                    Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleQuestion.quesImage).into(quesImg);
                                }
                                for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {

                                    if (i==0) {
                                        optionTxt1.setText(questionPuzzle.rafflPuzzleOption.get(i).optionValue);
                                    }
                                    if (i==1) {
                                        optionTxt2.setText(questionPuzzle.rafflPuzzleOption.get(i).optionValue);
                                    }
                                    if (i==2) {
                                        optionTxt3.setText(questionPuzzle.rafflPuzzleOption.get(i).optionValue);
                                    }
                                    if (i==3) {
                                        optionTxt4.setText(questionPuzzle.rafflPuzzleOption.get(i).optionValue);
                                    }



                                }


                            }

                            else if(questionPuzzle.optionFormat.equals("1")){
                                linearTxt.setVisibility(View.GONE);
                                linearImg.setVisibility(View.VISIBLE);
                                quesTxt1.setText(questionPuzzle.rafflPuzzleQuestion.quesText);
                                if (questionPuzzle.rafflPuzzleQuestion.quesImage.equals("") || questionPuzzle.rafflPuzzleQuestion.quesImage.equals("null")){
                                    quesImg.setVisibility(View.GONE);}
                                else {

                                    Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleQuestion.quesImage).into(quesImg);
                                }
                                for (int i = 0; i < questionPuzzle.rafflPuzzleOption.size(); i++) {
                                    if(i==0){
                                        Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleOption.get(i).optionValue).into(optImg1);


                                    }
                                    if(i==1){
                                        Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleOption.get(i).optionValue).into(optImg2);


                                    }
                                    if(i==2){
                                        Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleOption.get(i).optionValue).into(optImg3);


                                    }
                                    if(i==3){
                                        Glide.with(Question_Activity.this).load(questionPuzzle.rafflPuzzleOption.get(i).optionValue).into(optImg4);


                                    }

                                }


                            }
                            String text="By entering this competition you will have a minimum <font color=#a9d267><b>1/"+ questionPuzzle.totalToken+"</b></font> chance of winning this "+questionPuzzle.productName;
                            txt_ratio.setText(Html.fromHtml(text));


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              progressDialog.dismiss();
              Log.d("error",""+error);
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    public void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Question_Activity.this);
        View view = getLayoutInflater().inflate(R.layout.ticket_confirm_dialog, null);
        builder.setView(view);


        ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }
    public void showdialog1(final String noOfTickets, String optValue, String ticketPrice, String productName, int imageCount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Question_Activity.this);
        View view = getLayoutInflater().inflate(R.layout.ticket_confirm_dialog1, null);
        builder.setView(view);
        noOfTicketInt=Float.valueOf(noOfTickets);
         tic=Float.valueOf(ticketPrice);
        ticketPriceInt=Float.valueOf( tic);

     total_amount=ticketPriceInt*noOfTicketInt;

       cancel=(TextView)view.findViewById(R.id.cancel);
       confirm=(TextView)view.findViewById(R.id.confirm);
        txt1=(TextView)view.findViewById(R.id.txt1);
        txt2=(TextView)view.findViewById(R.id.txt2);
        txt3=(TextView)view.findViewById(R.id.txt3);
        txt1.setText("You're about to purchase "+noOfTickets+" ticket(s) to win the "+"'"+productName+".");
         if (questionPuzzle.optionFormat.equals("1")){

                     txt2.setText("Your answer to the question is option" + imageCount + ".");
         }
         else {
             txt2.setText("Your answer to the question is " + optValue + ".");
         }
        txt3.setText("Please confirm that this is a correct and your account will be debited with "+"£ "+total_amount+".");
         String ans=questionPuzzle.puzzleAnswer.answer;
        if (questionPuzzle.puzzleAnswer.answer.equals(optValue)){
            checkAnswerStaus="1";
        }
        else{
            checkAnswerStaus="0";
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyRaffleTicket(noOfTickets,checkAnswerStaus);
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }
    public void buyRaffleTicket(String noOfTickets, String checkAnswerStaus) {
        String url = APIConstantsDemo.BUY_RAFFLE_TOKEN;
        Log.i("url", url);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
            jsonObject.put("raffle_product_id",product_id);
            jsonObject.put("no_of_token",noOfTickets);
            jsonObject.put("total_amount",total_amount);
            jsonObject.put("puzzle_answer_status",checkAnswerStaus);
            jsonObject.put("payment_status","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {
                            buyRafflDialog(response.getString("message"));
                            dialogTitle.setText("Congratulations");
                            buttonNo.setVisibility(View.GONE);
                            buttonYes.setText("Ok");
                            IS_CHECK_SUCCESS=true;

                        }
                        else if(response.getString("status").equalsIgnoreCase("failed")){
                            buyRafflDialog("Insufficient Funds. Your Balance is £ "+response.getString("message"));
                            dialogTitle.setText("Insufficient fund");
                            IS_CHECK_SUCCESS=false;



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                  Log.d("error",""+error);
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    public void buyRafflDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Question_Activity.this);
        View view = getLayoutInflater().inflate(R.layout.logout_dialog, null);
        builder.setView(view);

         message = (TextView) view.findViewById(R.id.logoutText);
        buttonYes = (TextView) view.findViewById(R.id.buttonNo);
       buttonNo = (TextView) view.findViewById(R.id.buttonYes);
       dialogTitle = (TextView) view.findViewById(R.id.dialogTitle);

    buttonNo.setText(R.string.cancel);
    message.setText(msg);
    if(IS_CHECK_SUCCESS){
    buttonNo.setVisibility(View.GONE);
    buttonYes.setText(R.string.ok);
}
else {
        buttonYes.setText(R.string.My_account);
        buttonNo.setVisibility(View.VISIBLE);
        buttonNo.setText(R.string.cancel);

        }
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IS_CHECK_SUCCESS){

                    intent=new Intent(Question_Activity
                    .this,MainActivity.class);
                    startActivity(intent);
                    finish();

                }else {

                        intent=new Intent(Question_Activity
                                .this,MyAccountActivity.class);
                        startActivity(intent);
                }
                dialog.dismiss();


            }
        });
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();

        dialog.show();


    }
    public static String toFraction(double d, int factor) {
        StringBuilder sb = new StringBuilder();
        if (d < 0) {
            sb.append('-');
            d = -d;
        }
        long l = (long) d;
        if (l != 0) sb.append(l);
        d -= l;
        double error = Math.abs(d);
        int bestDenominator = 1;
        for(int i=2;i<=factor;i++) {
            double error2 = Math.abs(d - (double) Math.round(d * i) / i);
            if (error2 < error) {
                error = error2;
                bestDenominator = i;
            }
        }
        if (bestDenominator > 1)
            sb.append(' ').append(Math.round(d * bestDenominator)).append('/') .append(bestDenominator);
        return sb.toString();
    }

}
