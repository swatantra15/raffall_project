package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.Adapters.History_Adapter;
import com.yugasa.raffall.Adapters.SoldTicketItem;
import com.yugasa.raffall.HistoryDetails;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Raffle_HistoryActivity extends AppCompatActivity {
    History_Adapter history_adapter;
    TextView toolbarTitle;
    ImageView backIcon,shareIcon;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    String raffle_product_id;
    Intent intent;
    List<HistoryDetails> historyItems;
    TextView no_data_found;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raffle__history);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        toolbarTitle=(TextView) findViewById(R.id.toolbarTitle);
        backIcon=(ImageView) findViewById(R.id.back_icon);
        shareIcon=(ImageView) findViewById(R.id.share);
        no_data_found=(TextView) findViewById(R.id.no_data_text);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        shareIcon.setVisibility(View.GONE);
        toolbarTitle.setText("History");

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        historyItems=new ArrayList<>();
        intent=getIntent();
        if (intent!=null)
        raffle_product_id = intent.getStringExtra("r_p_Id");
        getHistoryDetails();
        history_adapter=new History_Adapter(historyItems,this);
        recyclerView.setAdapter(history_adapter);

    }
    public void getHistoryDetails(){
        String url= APIConstantsDemo.HISTORY_URL;
        Log.i("url",url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching history...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();


        try {
            jsonObject.put("raffle_product_id",raffle_product_id);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            JSONArray jsonArray = response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {


                                HistoryDetails historyDetails = new HistoryDetails();

                                JSONObject json1 = jsonArray.getJSONObject(i);

                                historyDetails.userName = json1.getString("user_name");
                                historyDetails.userImage = json1.getString("user_image");
                                historyDetails.userAddress = json1.getString("user_address");
                                historyDetails.productName = json1.getString("raffle_product_name");
                                historyDetails.raffalProductImage = json1.getString("raffle_product_image");
                                historyDetails.userRating = json1.getString("user_rating");
                                historyDetails.userReview = json1.getString("user_review");
                                historyDetails.soldToken = json1.getString("raffle_sold_token");
                                historyDetails.endDate = json1.getString("raffle_ending_date");
                                historyDetails.productTokenPrice = json1.getString("raffle_product_token_price");


                                historyItems.add(historyDetails);
                            }




                        }
                        history_adapter.notifyDataSetChanged();

                        if(historyItems.size()>0){
                            recyclerView.setVisibility(View.VISIBLE);
                            no_data_found.setVisibility(View.GONE);

                        }
                        else{
                            recyclerView.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }



}
