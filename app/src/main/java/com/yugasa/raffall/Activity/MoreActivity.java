package com.yugasa.raffall.Activity;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MoreItems;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.BindView;

public class MoreActivity extends AppCompatActivity {
    @BindView(R.id.toolbarTitle)
    TextView title;
    @BindView(R.id.likeFacebook)
    TextView likeUsOnFb;
    @BindView(R.id.follow_twitter)
    TextView followUsOnTwitter;
    @BindView(R.id.followInsta)
    TextView followUsOnInstagram;
    @BindView(R.id.followGoogle)
    TextView followUsOnGoogle;
    @BindView(R.id.back_icon)
    ImageView backIcon;
    @BindView(R.id.share)
    ImageView shareIcon;
    WebView contactDetails,faqDetails;
    List<MoreItems> moreitem;
    ProgressDialog progressDialog;
    String urlFb="https://www.facebook.com/RafflTicketApp/";
    String urlTwitter= "https://twitter.com/RafflTicketApp";
    String urlInstagram= "https://www.instagram.com/";
    String urlGoogle= "https://plus.google.com/people";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_more);

        ButterKnife.bind(this);
        moreitem=new ArrayList<>();

        faqDetails=(WebView)findViewById(R.id.faqTxt);
        contactDetails=(WebView)findViewById(R.id.contactTxt);




        title.setText(R.string.item_title8);
        shareIcon.setVisibility(View.GONE);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });
        getFaqDetails();

        likeUsOnFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlFb));
                startActivity(intent);
            }
        });

        followUsOnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlTwitter));
                startActivity(intent);
            }
        });

        followUsOnInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlInstagram));
                startActivity(intent);
            }
        });

        followUsOnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlGoogle));
                startActivity(intent);
            }
        });


    }
    public void getFaqDetails(){
        String url= APIConstantsDemo.MORE_URL;
        Log.i("url",url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching userinfo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("Success")){

                            JSONObject jsonObj=response.getJSONObject("data");
                            for (int i = 0; i < jsonObj.length(); i++) {
                                MoreItems moreItems=new MoreItems();
                                if (i==0){
                                    JSONObject json1=jsonObj.getJSONObject("raffle_faq");

                                   moreItems.id=json1.getString("id");
                                   moreItems.title=json1.getString("title");
                                   moreItems.description=json1.getString("description");
                                   moreItems.status=json1.getString("status");
                                   moreItems.modifiedOn=json1.getString("modified_on");
                                   moreItems.createdOn=json1.getString("created_on");
                                }
                                else if(i==1) {
                                    JSONObject json=jsonObj.getJSONObject("raffle_contact");
                                    moreItems.id = json.getString("id");
                                    moreItems.title = json.getString("title");
                                    moreItems.description = json.getString("description");
                                    moreItems.status = json.getString("status");
                                    moreItems.modifiedOn = json.getString("modified_on");
                                    moreItems.createdOn = json.getString("created_on");
                                }
                                moreitem.add(moreItems);

                            }
                           // Toast.makeText(MoreActivity.this, "listsize "+moreitem.size(), Toast.LENGTH_SHORT).show();
                            for (int i = 0; i <moreitem.size(); i++) {


                                if(i==0) {
                                    faqDetails.loadDataWithBaseURL("", moreitem.get(i).description, "text/html", "UTF-8", "");


                                }else if (i==1){
                                    contactDetails.loadDataWithBaseURL("", moreitem.get(i).description, "text/html", "UTF-8", "");

                                }

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


}