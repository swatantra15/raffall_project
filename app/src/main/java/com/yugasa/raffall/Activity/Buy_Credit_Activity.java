package com.yugasa.raffall.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.yugasa.raffall.Adapters.Custom_CreditValue_Adapter;
import com.yugasa.raffall.CreditValues;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.PaypalItem;
import com.yugasa.raffall.Paypal_config;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Buy_Credit_Activity extends AppCompatActivity {
    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.back_icon)
    ImageView backIcon;
    @BindView(R.id.share)
    ImageView shareIcon;
    @BindView(R.id.buy_btn)
    TextView buyButton;
    public static final int PAYPAL_REQUEST_CODE = 123;
    String paymentAmount;
    List<CreditValues> credits;
    ProgressDialog progressDialog;
    private static PayPalConfiguration config;
    List<String> creditValues;



    @BindView(R.id.listview)
   ListView listView;

  // String credit[]={"GBP 1.00","GBP 2.00","GBP 5.00","GBP 10.00","GBP 15.00","GBP 20.00","GBP 50.00","GBP 100.00"};
  // String credit[]={"1","2","5","10","15","20"};
   Custom_CreditValue_Adapter adapter;
    String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy__credit_);
        ButterKnife.bind(this);
        ButterKnife.bind(this);
        credits=new ArrayList<>();


        getCreditValue();




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
               amount=credits.get(position).creditAmountValue;
                view.setSelected(true);

              //  listView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.app_background));
                buyButton.setVisibility(View.VISIBLE);

            }

//                        listView.getChildAt(1).setBackgroundColor(Color.BLUE);
//                        firstListItemPosition = firstListItemPosition +1;




              // String firstListItemPosition = listView.getFirstVisiblePosition();



        });


        toolbarTitle.setText("Buy Credit");
        shareIcon.setVisibility(View.GONE);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        config = new PayPalConfiguration()
                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(Paypal_config.PAYPAL_CLIENT_ID);
        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startService(intent);

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPayment();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);


                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = null;
                        JSONObject jsonObject=null;
//                        response =     {
//                                "create_time" = "2017-11-10T12:32:33Z";
//                        id = "PAY-NONETWORKPAYIDEXAMPLE123";
//                        intent = sale;
//                        state = approved;
//    };

                        jsonObject = confirm.toJSONObject();
                        if(jsonObject!=null && jsonObject.length()>0){
                            PaypalItem paypalItem=new PaypalItem();
                            JSONObject response=jsonObject.getJSONObject("response");
                            paypalItem.create_time= response.getString("create_time");
                            paypalItem.paypalId=response.getString("id");
                            paypalItem.intent=response.getString("intent");
                            paypalItem.status=response.getString("state");
                            getCreditEntry(paypalItem);
                        }

//                        Log.i("paymentExample", paymentDetails);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(this, "success"+amount, Toast.LENGTH_SHORT).show();

                    //Starting a new activity for the payment details and also putting the payment details with intent
                      /*  startActivity(new Intent(this, ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", paymentAmount));
*/

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void getPayment() {
        //Getting the amount from editText
       // amount=amount.substring(4);
        //amount=amount.substring(0,amount.indexOf("."));
       // amount= Arrays.toString(arr);
//        paymentAmount=arr;

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "GBP", "Raffl Ticket Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config );

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }


    public void getCreditValue() {
        String url = APIConstantsDemo.CREDIT_URL;
        Log.i("url", url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            JSONArray jsonArray = response.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonobj1=jsonArray.getJSONObject(i);

                                CreditValues creditValues=new CreditValues();


                                creditValues.amountId = jsonobj1.getString("credit_amount_id");
                                creditValues.creditAmountValue = jsonobj1.getString("credit_amount_value");
                                credits.add(creditValues);
                            }

                            adapter=new Custom_CreditValue_Adapter(credits);

                            listView.setAdapter(adapter);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("volly",""+error);

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));



    }
    public void getCreditEntry(PaypalItem paypalItem) {
        String url = APIConstantsDemo.CREDIT_ENTRY;
        Log.i("url", url);
        UserInfo userInfo = DroidPrefs.get(this, "user_info", UserInfo.class);
       /* progressDialog = new ProgressDialog(this);
       user_id, paypal_id, credit_amount, intent, status, create_time, user_token
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userInfo.userId);
            jsonObject.put("paypal_id",paypalItem.paypalId);
            jsonObject.put("intent", paypalItem.intent);
            jsonObject.put("status", paypalItem.status);
            jsonObject.put("create_time", paypalItem.create_time);
            jsonObject.put("credit_amount",amount);
            jsonObject.put("user_token", userInfo.userToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {
                            Toast.makeText(Buy_Credit_Activity.this, response.getString("message"),Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
    }

