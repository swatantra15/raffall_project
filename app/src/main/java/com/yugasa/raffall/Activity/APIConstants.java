package com.yugasa.raffall.Activity;

/**
 * Created by yugasalabs-45 on 13/9/17.
 */

public class APIConstants {
    public static final String BASE_URL="http://203.92.41.131/raffle/api/get-raffles";
    public static final String PRODUCT_INFO_URL="http://203.92.41.131/raffle/api/get-raffle-information";
    public static final String MORE_URL="http://203.92.41.131/raffle/api/get-raffle-faq-and-contacts";
    public static final String Local_URL="http://192.168.1.7/raffle/api/get-raffles";
    public static final String RULES_URL="http://203.92.41.131/raffle/api/get-raffle-rules-guidelines";
    public static final String TERMS_URL="http://203.92.41.131/raffle/api/get-raffle-terms-services";
    public static final String POLICY_URL="http://203.92.41.131/raffle/api/get-raffle-privacy-policy";
    public static final String QUESURL="http://203.92.41.131/raffle/api/get-raffle-puzzle";
    public static final String SOLD_URL="http://203.92.41.131/raffle/api/get-sold-tickets";
    public static final String REFFERAL_URL="http://203.92.41.131/raffle/api/get-user-referral-code";
    public static final String WINNER_URL="http://203.92.41.131/raffle/api/get-recent-winners";
        public static final String HISTORY_URL="http://203.92.41.131/raffle/api/get-raffle-history";
        public static final String MYRAFFLE_URL="http://203.92.41.131/raffle/api/get-my-raffles";
        public static final String CATEGORY_URL="http://203.92.41.131/raffle/api/get-categories";
        public static final String ACCOUNT_URL="http://203.92.41.131/raffle/api/get-user-account-info";
        public static final String CREDIT_URL="http://203.92.41.131/raffle/api/get-credit-amount-values";
        public static final String LOGIN_URL="http://203.92.41.131/raffle/api/raffle-user-login";
        public static final String LOGOUT_URL="http://203.92.41.131/raffle/api/raffle-user-logout";
        public static final String FRIEND_REFFERAL_URL="http://203.92.41.131/raffle/api/manupulate-friend-referral-code";
        public static final String OTHER_SOCIAL_LOGIN_URL="http://203.92.41.131/raffle/api/link-social-account";
        public static final String ACCOUNT_HISTORY="http://203.92.41.131/raffle/api/get-user-credit-history";
        public static final String CATEGORY_ITEM="http://203.92.41.131/raffle/api/get-raffles-on-category";
        public static final String SUBMIT_REVIEW="http://203.92.41.131/raffle/api/submit-winner-rating";
        public static final String BUY_RAFFLE_TOKEN="http://203.92.41.131/raffle/api/buy-raffl-token";
        public static final String BADGE_COUNT="http://203.92.41.131/raffle/api/set-badge-count-zero";
        public static final String CREDIT_ENTRY="http://203.92.41.131/raffle/api/user-credit-entry";
}
