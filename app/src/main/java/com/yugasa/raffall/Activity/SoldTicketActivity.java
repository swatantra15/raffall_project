package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.Adapters.SoldTicketItem;
import com.yugasa.raffall.Adapters.Sold_Ticket_Adapter;
import com.yugasa.raffall.MoreItems;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.ProductDetails;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;
import com.yugasa.raffall.Utils.ProductItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SoldTicketActivity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView backIcon,shareIcon;
    TextView toolbarTitle;
    String raffle_product_id;
    RecyclerView recyclerView;
    Sold_Ticket_Adapter adapter;
    List<SoldTicketItem> soldTicketItems;
    ProgressDialog progressDialog;
    Intent intent;
    ImageLoader imageLoader;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sold_ticket);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        toolbarTitle=(TextView) findViewById(R.id.toolbarTitle);
        shareIcon=(ImageView) findViewById(R.id.share);
        recyclerView=(RecyclerView) findViewById(R.id.recyclerView1);

    shareIcon.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        intent=getIntent();
        if(intent!=null) {

            raffle_product_id = intent.getStringExtra("r_p_Id");
        }

        toolbarTitle.setText("Sold Ticket");



        soldTicketItems=new ArrayList<>();
        getSoldTicketDetails();

        adapter=new Sold_Ticket_Adapter(soldTicketItems,this);

        recyclerView.setAdapter(adapter);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }
    public void getSoldTicketDetails(){
        String url= APIConstantsDemo.SOLD_URL;
        Log.i("url",url);
        UserInfo userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching userinfo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();


        try {
            jsonObject.put("raffle_product_id",raffle_product_id);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){

                            JSONArray jsonArray=response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                SoldTicketItem soldTicketItem = new SoldTicketItem();

                                    JSONObject json1 = jsonArray.getJSONObject(i);

                                    soldTicketItem.buyerName = json1.getString("user_name");
                                    soldTicketItem.imageView1 = json1.getString("user_image");
                                    soldTicketItem.country = json1.getString("user_address");
                                    soldTicketItem.totalbuyTicket = json1.getString("no_of_token");


                                soldTicketItems.add(soldTicketItem);

                            }


                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


}
