package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PrivacyPolicyActivity extends AppCompatActivity {
    WebView policyTxt;
    ProgressDialog progressDialog;
    List<RulesItem> policyItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        policyTxt=(WebView)findViewById(R.id.policyTxt);
        policyItems=new ArrayList<>();
        getPrivacyAndPolicyData();
    }



    public void getPrivacyAndPolicyData(){
        String url= APIConstantsDemo.POLICY_URL;
        Log.i("url",url);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("Success")){

                            JSONObject jsonObj=response.getJSONObject("data");

                            RulesItem rulesItem=new RulesItem();



                            rulesItem.id=jsonObj.getString("id");
                            rulesItem.title=jsonObj.getString("title");
                            rulesItem.description=jsonObj.getString("description");






                         //   Toast.makeText(PrivacyPolicyActivity.this, "listsize "+policyItems.size(), Toast.LENGTH_SHORT).show();



                            //  termsText.setText(Html.fromHtml(rulesItem.description));


                            try {
                                policyTxt.loadDataWithBaseURL("",rulesItem.description,"text/html","UTF-8","");


                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
}
