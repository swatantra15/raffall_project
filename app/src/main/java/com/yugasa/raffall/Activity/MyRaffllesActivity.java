package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.Adapters.My_Raffle_Adapter;
import com.yugasa.raffall.Adapters.SoldTicketItem;
import com.yugasa.raffall.MyRaffle;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyRaffllesActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    My_Raffle_Adapter myRaffleAdapter;
    ImageView backIcon,shareIcon;
    String user_id;
  public List<MyRaffle> myRaffleList;
    ProgressDialog progressDialog;
    ImageLoader imageLoader;
    TextView toolbarTitle,noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_rafflles);
        backIcon=(ImageView)findViewById(R.id.back_icon);
        shareIcon=(ImageView)findViewById(R.id.share);
        toolbarTitle=(TextView) findViewById(R.id.toolbarTitle);
        noDataText=(TextView) findViewById(R.id.no_data_text);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        shareIcon.setVisibility(View.GONE);
        toolbarTitle.setText(R.string.item_title4);

        myRaffleList=new ArrayList<>();
        getMyRaffleDetails();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));




        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void getMyRaffleDetails(){
        String url= APIConstantsDemo.MYRAFFLE_URL;
        Log.i("url",url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Fetching userinfo...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject=new JSONObject();


        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")){

                            JSONArray jsonArray=response.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                MyRaffle myRaffle=new MyRaffle();

                                JSONObject json1 = jsonArray.getJSONObject(i);



                                myRaffle.productName = json1.getString("product_name");
                                myRaffle.productImage = json1.getString("raffle_product_image");
                                myRaffle.raffleProductId = json1.getString("raffle_product_id");
                                myRaffle.tokenPrice = json1.getString("token_price");
                                myRaffle.totalToken = json1.getString("total_token");
                                myRaffle.soldToken = json1.getString("sold_token");
                                myRaffle.freeToken = json1.getString("available_token");
                                myRaffle.endCondition = json1.getString("raffle_end_condition");
                                myRaffle.no_of_token = json1.getString("no_of_token");
                                myRaffle.runningStatus = json1.getString("running_status");



                                myRaffleList.add(myRaffle);

                            }
                            if(myRaffleList.size()>0){
                                recyclerView.setVisibility(View.VISIBLE);
                                noDataText.setVisibility(View.GONE);

                            }
                            else{
                                recyclerView.setVisibility(View.GONE);
                                noDataText.setVisibility(View.VISIBLE);

                            }


                        }
                       if(myRaffleList.size()>0){
                           myRaffleAdapter=new My_Raffle_Adapter(myRaffleList,MyRaffllesActivity.this);
                           recyclerView.setAdapter(myRaffleAdapter);
                       }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


}
