package com.yugasa.raffall.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.yugasa.raffall.AccountHistory;
import com.yugasa.raffall.Account_Info;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.BindView;

public class MyAccountActivity extends AppCompatActivity {
    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.back_icon)
    ImageView backIcon;
    @BindView(R.id.share)
    ImageView shareIcon;
    @BindView(R.id.historyIcon)
    ImageView historyIcon;
    @BindView(R.id.buyCredit)
    TextView buyCredit;
    ProgressDialog progressDialog;
    List<Account_Info> account_infoList;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.no_of_refferals)
    TextView noOfRefferals;
    public  String acc;
    @BindView(R.id.refferalRevenueEarned)
    TextView refferalRevenuEarned;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_raffal_account);
        ButterKnife.bind(this);

        toolbarTitle.setText(R.string.item_title3);
        shareIcon.setVisibility(View.GONE);

        getAccountInfo();




        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        buyCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAccountActivity.this, Buy_Credit_Activity.class);
                startActivity(intent);
            }
        });

        historyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccountActivity.this,Account_History.class);
                startActivity(intent);

            }
        });



    }
    public void getAccountInfo() {
        String url = APIConstantsDemo.ACCOUNT_URL;
        Log.i("url", url);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        UserInfo userInfo = DroidPrefs.get(this,"user_info",UserInfo.class);
        try {
            jsonObject.put("user_id",userInfo.userId);
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            JSONObject jsonObj = response.getJSONObject("data");

                            Account_Info account_info=new Account_Info();


                            account_info.id = jsonObj.getString("id");
                            account_info.userId = jsonObj.getString("user_id");
                            account_info.balance = jsonObj.getString("balance");
                            account_info.noOfReferrals = jsonObj.getString("no_of_referrals");
                            account_info.referralRevenueEarned = jsonObj.getString("referral_revenue_earned");
                            account_info.createdOn = jsonObj.getString("referral_revenue_earned");


                           balance.setText("£ "+account_info.balance);
                           noOfRefferals.setText(account_info.noOfReferrals);
                           refferalRevenuEarned.setText("£ "+account_info.referralRevenueEarned);

                            DroidPrefs.apply(MyAccountActivity.this,"account_info",account_info);




                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

}
