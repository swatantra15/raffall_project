package com.yugasa.raffall.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.yugasa.raffall.MoreItems;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.RulesItem;
import com.yugasa.raffall.UserInfo;
import com.yugasa.raffall.Utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RulesAndGuidelines extends AppCompatActivity {
    ProgressDialog progressDialog;
    List<RulesItem> RulesItm;
    TextView toolbarTitle;
    WebView rulesTxt;
    ImageView backIcon,shareIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_and_guidelines);
        rulesTxt = (WebView) findViewById(R.id.rules);
        backIcon = (ImageView) findViewById(R.id.back_icon);
        shareIcon = (ImageView) findViewById(R.id.share);
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        shareIcon.setVisibility(View.GONE);

        getRulesAndGuidlines();
        toolbarTitle.setText(R.string.item_title7);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RulesItm = new ArrayList<>();
    }

    public void getRulesAndGuidlines() {
        String url = APIConstantsDemo.RULES_URL;
        Log.i("url", url);
        UserInfo userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_token",userInfo.userToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        if (response.getString("status").equalsIgnoreCase("Success")) {

                            JSONObject jsonObj = response.getJSONObject("data");

                            RulesItem rulesItem = new RulesItem();


                            rulesItem.id = jsonObj.getString("id");
                            rulesItem.title = jsonObj.getString("title");
                            rulesItem.description = jsonObj.getString("description");


                          //  Toast.makeText(RulesAndGuidelines.this, "listsize " + RulesItm.size(), Toast.LENGTH_SHORT).show();


                            //  rulesTxt .setText(Html.fromHtml(rulesItem.description));

                            try {
                                rulesTxt.loadDataWithBaseURL("", rulesItem.description, "text/html", "UTF-8", "");


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }
}