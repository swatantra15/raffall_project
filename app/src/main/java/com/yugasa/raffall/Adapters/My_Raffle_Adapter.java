package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.yugasa.raffall.MyRaffle;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;

import java.util.List;

/**
 * Created by yugasalabs-45 on 3/10/17.
 */

public class My_Raffle_Adapter extends RecyclerView.Adapter<My_Raffle_Adapter.MyViewHolder>  {
  public   List<MyRaffle> myRaffleList;
    Context context;
    ImageLoader imageLoader;
    public My_Raffle_Adapter(List<MyRaffle> myRaffleList, Context context) {
        this.myRaffleList = myRaffleList;
        this.context = context;

    }



    @Override
    public My_Raffle_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        imageLoader= MySingleton.getInstance(parent.getContext()).getImageLoader();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_raffle_item,parent,false);
        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final My_Raffle_Adapter.MyViewHolder holder, int position) {

        holder.productName.setText(myRaffleList.get(position).productName);
        holder.ticketPrice.setText("£ "+myRaffleList.get(position).tokenPrice);
        holder.totalTicket.setText(myRaffleList.get(position).totalToken);
        holder.soldTicket.setText(myRaffleList.get(position).soldToken);
        holder.freeTicket.setText(myRaffleList.get(position).freeToken);
        holder.endCondition.setText(myRaffleList.get(position).endCondition);
        if(myRaffleList.get(position).runningStatus.equals("0")) {
        holder.noOfTokens.setTextColor(context.getResources().getColor(R.color.ticket_sold));
            holder.noOfTokens.setBackground(context.getResources().getDrawable(R.drawable.redbar_icon));
        }
        else  if(myRaffleList.get(position).runningStatus.equals("1")){
            holder.noOfTokens.setBackground(context.getResources().getDrawable(R.drawable.bar_icon));
            holder.noOfTokens.setTextColor(context.getResources().getColor(R.color.bar_icon_green));
        }
        holder.noOfTokens.setText("No. of tickets purchased: "+myRaffleList.get(position).no_of_token);


        imageLoader.get(myRaffleList.get(position).productImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    Bitmap bmp=response.getBitmap();
                    holder.productImage.setImageBitmap(response.getBitmap());

                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("err",""+error);
            }
        });


    }

    @Override
    public int getItemCount() {
        return myRaffleList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ticketPrice,totalTicket,soldTicket,freeTicket,productName,endCondition,noOfTokens;
        ImageView productImage;
        public MyViewHolder(View itemView) {
            super(itemView);
            ticketPrice=(TextView)itemView.findViewById(R.id.tokenPrice);
            totalTicket=(TextView)itemView.findViewById(R.id.totalTicket);
            soldTicket=(TextView)itemView.findViewById(R.id.totalSold);
            freeTicket=(TextView) itemView.findViewById(R.id.totalFree);
            productName=(TextView) itemView.findViewById(R.id.productname);
            endCondition=(TextView) itemView.findViewById(R.id.endCondition);
            noOfTokens=(TextView) itemView.findViewById(R.id.no_of_tokens);
            productImage=(ImageView) itemView.findViewById(R.id.productImage);
        }
    }
}
