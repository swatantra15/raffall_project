package com.yugasa.raffall.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yugasa.raffall.AccountHistory;
import com.yugasa.raffall.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yugasalabs-45 on 12/10/17.
 */

public class AccountHistoryAdapter extends RecyclerView.Adapter<AccountHistoryAdapter.MyViewHolder> {
    public AccountHistoryAdapter(List<AccountHistory> accountHistoryList) {
        this.accountHistoryList = accountHistoryList;
    }

    List<AccountHistory> accountHistoryList;
    TextView date,amount,reason;



    @Override
    public AccountHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_history_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override

    public void onBindViewHolder(AccountHistoryAdapter.MyViewHolder holder, int position) {


        date.setText(accountHistoryList.get(position).create_time);
        reason.setText(accountHistoryList.get(position).reason);


        if (accountHistoryList.get(position).type.equals("0") ) {
            amount.setText(("- £ " + accountHistoryList.get(position).credit_amount) + " db");
        } else if (accountHistoryList.get(position).type.equals("1")){

            amount.setText(("+ £ " + accountHistoryList.get(position).credit_amount) + " cr");
        }


    }


    @Override
    public int getItemCount() {
        return accountHistoryList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView historyItem;

        public MyViewHolder(View itemView) {
            super(itemView);

            date=(TextView)itemView.findViewById(R.id.date);
            reason=(TextView)itemView.findViewById(R.id.reason);
            amount=(TextView)itemView.findViewById(R.id.amount);


        }
    }}
