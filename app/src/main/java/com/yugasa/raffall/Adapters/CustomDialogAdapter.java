package com.yugasa.raffall.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.CustomDialogEntity;

import java.util.ArrayList;

/**
 * Created by mohit on 29/12/15.
 */
public class CustomDialogAdapter extends RecyclerView.Adapter<CustomDialogAdapter.ViewHolder> {
    private ArrayList<CustomDialogEntity> list;
    Share share;
    public interface  Share{
        void OnShare(View view, int position);
    }
    public CustomDialogAdapter(ArrayList<CustomDialogEntity> list,Share share){
        this.list=list;
        this.share=share;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refer_recycler_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CustomDialogEntity referEntity=list.get(position);

        holder.image.setImageDrawable(referEntity.image);
        holder.text.setText(referEntity.name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(share!=null){
                    share.OnShare(v,position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView text;
        LinearLayout mainLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            image=(ImageView)itemView.findViewById(R.id.refer_image);
            text=(TextView)itemView.findViewById(R.id.refer_text);
            mainLayout=(LinearLayout)itemView.findViewById(R.id.refer_item_mainLayout);
        }
    }
}
