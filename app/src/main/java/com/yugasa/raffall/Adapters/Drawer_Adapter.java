package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.Items.DrawerItem;
import com.yugasa.raffall.Utils.Items.ViewHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by yugasalabs-45 on 1/9/17.
 */

public class Drawer_Adapter extends RecyclerView.Adapter<Drawer_Adapter.MyViewHolder>{
    List<DrawerItem> drawerItemList = Collections.emptyList();
    Context context;
    Clicked clicked;





    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.title.setText(drawerItemList.get(position).title);
        holder.description.setText(drawerItemList.get(position).description);
        holder.imageView.setImageResource(drawerItemList.get(position).imageId);
        holder.drawerLayout.setTag(position);

        holder.drawerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clicked != null) {
                    int pos= (int) view.getTag();
                    clicked.Click(view, pos);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawerItemList.size();
    }

    public interface Clicked {
        void Click(View view, int position);
    }


    public Drawer_Adapter(List<DrawerItem> drawerItemList, Context context, Clicked clicked) {

        this.drawerItemList = drawerItemList;
        this.context = context;
        this.clicked = clicked;
    }






    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;
        ImageView imageView;
        LinearLayout drawerLayout;


        public MyViewHolder(View itemView) {
            super(itemView);
            title= itemView.findViewById(R.id.textViewTitle);
            description=itemView.findViewById(R.id.textViewDescription);
            imageView= itemView.findViewById(R.id.imageviewItem);
            drawerLayout=itemView.findViewById(R.id.drawer_layout);

        }
    }
    public void insert(int position, DrawerItem data) {

        drawerItemList.add(position, data);

        notifyItemInserted(position);

    }

    public void remove(DrawerItem data) {

        int position = drawerItemList.indexOf(data);

        drawerItemList.remove(position);
        notifyItemRemoved(position);
    }
}
