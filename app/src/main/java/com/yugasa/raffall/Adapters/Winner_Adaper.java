package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.yugasa.raffall.Activity.WinnerActivity;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.WinnerItem;

import java.util.List;

/**
 * Created by yugasalabs-45 on 26/9/17.
 */

public class Winner_Adaper extends RecyclerView.Adapter<Winner_Adaper.MyViewHolder> {
    Context context;
    List<WinnerItem> winnerItems;
    ImageLoader imageLoader;

    public Winner_Adaper(Context context, List<WinnerItem> winnerItems) {
        this.context = context;
        this.winnerItems = winnerItems;
        imageLoader= MySingleton.getInstance(context).getImageLoader();
    }

    public Winner_Adaper() {
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.raffle_winner_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.providerName.setText(winnerItems.get(position).providerName);

        if (winnerItems.get(position).userReview!=null && !winnerItems.get(position).userReview.equals("null")) {
        holder.userReview.setText(winnerItems.get(position).userReview);
        }else
            holder.userReview.setText("Awaiting Review..");

        holder.providerName.setText(winnerItems.get(position).providerName);
        holder.wonDate.setText("Won on "+winnerItems.get(position).winningDate);
        holder.username.setText(winnerItems.get(position).username);
        if (winnerItems.get(position).address!=null && !winnerItems.get(position).address.equals("null")) {
        holder.address.setText(winnerItems.get(position).address);
        }else {
            holder.address.setText("");
        }
        if (winnerItems.get(position).userRating!=null && !winnerItems.get(position).userRating.equals("null")) {
            holder.ratingBar.setRating(Float.parseFloat(winnerItems.get(position).userRating));
        }else {
            holder.ratingBar.setRating(Float.parseFloat("0"));
        }





        imageLoader.get(winnerItems.get(position).productImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.productImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        imageLoader.get(winnerItems.get(position).user_image, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.userImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        imageLoader.get(winnerItems.get(position).providerImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.providerImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


    }



    @Override
    public int getItemCount() {
        return winnerItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView providerName,username,address,userReview,wonDate ;
            ImageView productImage,providerImage,userImage;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            providerName=(TextView)itemView.findViewById(R.id.providerName);
            providerImage=(ImageView) itemView.findViewById(R.id.provider_logo);
            productImage=(ImageView) itemView.findViewById(R.id.product_image);
            userImage=(ImageView) itemView.findViewById(R.id.userImage);
            userReview=(TextView) itemView.findViewById(R.id.user_review);
            username=(TextView) itemView.findViewById(R.id.username_txt);
            address=(TextView) itemView.findViewById(R.id.address_txt);
            wonDate=(TextView) itemView.findViewById(R.id.won_date);
            ratingBar=(RatingBar) itemView.findViewById(R.id.ratingBar);



        }
    }
}
