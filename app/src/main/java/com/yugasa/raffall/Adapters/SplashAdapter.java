package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.SplashItem;

import java.util.ArrayList;

/**
 * Created by yugasalabs-45 on 25/8/17.
 */

public class SplashAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<SplashItem> splashItm;

    public SplashAdapter(Context context, ArrayList<SplashItem> splashItm) {
        this.context = context;
        this.splashItm = splashItm;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.splash_screen_1, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.img_splash1);
        final TextView textView=(TextView)imageLayout.findViewById(R.id.splash_text1);
        textView.setText(splashItm.get(position).getTitle());


        imageView.setImageResource(splashItm.get(position).getDrawable());

        view.addView(imageLayout, 0);


        return imageLayout;
    }




    @Override
    public int getCount() {
        return splashItm.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return  view.equals(object);
    }
}
