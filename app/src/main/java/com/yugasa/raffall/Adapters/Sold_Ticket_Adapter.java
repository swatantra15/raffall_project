package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.Items.DrawerItem;

import java.util.Collections;
import java.util.List;

/**
 * Created by yugasalabs-45 on 25/9/17.
 */

public class Sold_Ticket_Adapter extends RecyclerView.Adapter<Sold_Ticket_Adapter.MyViewHolder> {
    List<SoldTicketItem> soldItemList ;
    ImageLoader imageLoader;

    public Sold_Ticket_Adapter(List<SoldTicketItem> soldItemList, Context context) {
        this.soldItemList = soldItemList;
        this.context = context;
        imageLoader= MySingleton.getInstance(context).getImageLoader();
    }

    Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sold_ticket_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.buyerName.setText(soldItemList.get(position).buyerName);
        if (soldItemList.get(position).country!=null && !soldItemList.get(position).country.equals("null")) {
            holder.country.setText(soldItemList.get(position).country);
        }
        else{
            holder.country.setText("UK");
        }
        holder.totalbuyTicket.setText("x"+soldItemList.get(position).totalbuyTicket);
//        Glide.with(context).load(soldItemList.get(position).imageView1).into(holder.imageView);

        imageLoader.get(soldItemList.get(position).imageView1, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.imageView.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                 Log.d("err",""+error);
            }
        });

    }

    @Override
    public int getItemCount() {
        return soldItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView buyerName,country,totalbuyTicket;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            buyerName=(TextView)itemView.findViewById(R.id.buyer_name);
            country=(TextView)itemView.findViewById(R.id.counryName);
            totalbuyTicket=(TextView)itemView.findViewById(R.id.totalbuytTicket);
            imageView=(ImageView) itemView.findViewById(R.id.buyer_image);
        }
    }
}
