package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.yugasa.raffall.Activity.MainActivity;
import com.yugasa.raffall.Activity.Question_Activity;
import com.yugasa.raffall.R;
import com.yugasa.raffall.Utils.ProductItems;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yugasalabs-45 on 6/9/17.
 */

public class Swipe_Deck_Adapter extends ArrayAdapter<ProductItems>{
    LayoutInflater inflater;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    TextView webLink;

    Button close;
    Bitmap default_bitmap;
    private List<ProductItems> data;
    private Context context;

    ImageLoader imageLoader;
    ProductItems productItems;
    String productId;

    public String url="http://203.92.41.131/raffle/webroot/img/raffle_product_images/xIJ2nlis-watch.jpg";



    public Swipe_Deck_Adapter( Context context) {
        super(context,0);
        this.context=context;


    }





    @Override
    public View getView(final int position, View contentView, ViewGroup parent) {

        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.swipe_deck_item, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }



          productItems=getItem(position);
        Glide.with(getContext()).load(productItems.productImage).into(holder.productImg);
        Glide.with(getContext()).load(productItems.providerImage).into(holder.productLogo);
        holder.ticketTxt.setText("£ "+productItems.tokenPrice);
        holder.totalTxt.setText(productItems.totalToken);
        holder.soldTxt.setText(productItems.soldToken);
        holder.freeTxt.setText(productItems.freeToken);
        holder.productName.setText(productItems.productName);
        holder.country.setText(productItems.address);
        productId=productItems.productId;

        holder.raffalProvider.setText(productItems.raffleProvider);



        holder.playpuzzle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(context, Question_Activity.class);

                intent.putExtra("productID",productId);

                intent.putExtra("freeTicket",productItems.freeToken);
                context.startActivity(intent);*/
                ((MainActivity)context).clickPlayButton();
            }
        });


       /* contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Card_ItemActivity.class);
                intent.putExtra("productID",productItems.productId);
               // intent.putParcelableArrayListExtra("data", (ArrayList<? extends Parcelable>) data);
               // intent.putExtra("pos",position);
                context.startActivity(intent);


             //   String item = (String)getItem(position);
               // Log.i("MainActivity", item);
            }
        });*/
        holder.bottomClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                showDialog(position);
            }
        });


        return contentView;
    }


    public void showDialog(int position) {
        productItems=getItem(position);
        bottomSheetDialog = new BottomSheetDialog(context);
        View v = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_dialog_popup, null);
        bottomSheetDialog.setContentView(v);
        bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());

       final TextView webLink=(TextView)v.findViewById(R.id.webLink);
       Button close=(Button)v.findViewById(R.id.clickDialog);
       TextView raffalProvider=(TextView)v.findViewById(R.id.productBrand);
       final CircleImageView productLogo=(CircleImageView)v.findViewById(R.id.productlogo);
       TextView productName=(TextView)v.findViewById(R.id.productName);
      TextView  description=(TextView)v.findViewById(R.id.description);

        description.setText(productItems.aboutRaffleProvider);
        webLink.setText(Html.fromHtml("<u>"+productItems.raffleProviderWeblink+"</u>"));
        raffalProvider.setText(productItems.raffleProvider);
          Glide.with(getContext()).load(productItems.providerImage).into(productLogo);
       /* imageLoader.get(productItems\.providerImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    productLogo.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });*/
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
webLink.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse("http://"+webLink.getText().toString());
        i.setData(uri);
        context.startActivity(i);
    }
});
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    bottomSheetDialog.dismiss();
                }
            }
        });
     //   bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();
        bottomSheetDialog.setCanceledOnTouchOutside(false);




    }
    private static class ViewHolder {
        private TextView ticketTxt,totalTxt,soldTxt,freeTxt,productName,country,raffalProvider,description,playpuzzle;
        private ImageView productImg,productLogo;
        LinearLayout bottomClick;

        public ViewHolder(View v) {
            ticketTxt=(TextView) v.findViewById(R.id.ticketPric);
            totalTxt=(TextView) v.findViewById(R.id.totalTicket);
            soldTxt=(TextView) v.findViewById(R.id.totalSold);
            totalTxt=(TextView) v.findViewById(R.id.totalTicket);
            freeTxt=(TextView) v.findViewById(R.id.totalFree);
            productImg=(ImageView)v.findViewById(R.id.productImage);
            productName=(TextView) v.findViewById(R.id.productName);
            playpuzzle=(TextView) v.findViewById(R.id.play);
            productLogo=(ImageView) v.findViewById(R.id.product_logo);
            country=(TextView) v.findViewById(R.id.counryName);
            raffalProvider=(TextView)v.findViewById(R.id.productBrand);
            bottomClick=(LinearLayout)v.findViewById(R.id.bottomClick);
        }
    }

}
