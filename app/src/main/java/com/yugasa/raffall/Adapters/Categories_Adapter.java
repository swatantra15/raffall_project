package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.yugasa.raffall.CategoryList;
import com.yugasa.raffall.R;

import java.util.List;

/**
 * Created by yugasalabs-45 on 4/10/17.
 */

public class Categories_Adapter extends RecyclerView.Adapter<Categories_Adapter.MyViewHolder> {
    List<CategoryList> categoryItems;
    public  interface CategoryClick{
        void Clicked(View view,int position);
    }
    Context context;
    CategoryClick categoryClick;

    public Categories_Adapter(List<CategoryList> categoryItems, Context context,CategoryClick categoryClick) {
        this.categoryItems = categoryItems;
        this.context = context;
        this.categoryClick=categoryClick;
    }

    public Categories_Adapter(){}
    @Override
    public Categories_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Categories_Adapter.MyViewHolder holder, int position) {
        holder.categoryItem.setText(categoryItems.get(position).categoryName);
        holder.drawer_layout.setTag(position);
        holder.drawer_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categoryClick!=null){
                    int pos= (int) v.getTag();
                    categoryClick.Clicked(v,pos);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryItems.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryItem;
        LinearLayout drawer_layout;
        public MyViewHolder(View itemView) {
            super(itemView);
            categoryItem=(TextView)itemView.findViewById(R.id.categoryItem);
            drawer_layout=itemView.findViewById(R.id.drawer_layout);


        }

    }
}
