package com.yugasa.raffall.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yugasa.raffall.CreditValues;
import com.yugasa.raffall.R;

import java.util.List;

/**
 * Created by yugasalabs-45 on 13/10/17.
 */

public class Custom_CreditValue_Adapter extends BaseAdapter {
    TextView creditValue;
    public Custom_CreditValue_Adapter(List<CreditValues> creditValuesList) {
        this.creditValuesList = creditValuesList;
    }

    List<CreditValues> creditValuesList;


    @Override
    public int getCount() {
        return creditValuesList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.buy_credit_list_item, parent, false);
        creditValue=(TextView)convertView.findViewById(R.id.creditItem);

        creditValue.setText("GBP "+creditValuesList.get(position).creditAmountValue);

        return convertView;
    }
}
