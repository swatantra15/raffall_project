package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;

import java.util.List;

/**
 * Created by yugasalabs-45 on 19/9/17.
 */

public class Product_Image_Adapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;
    List<String> imageItm;
    ImageLoader imageLoader;
    Bitmap default_bitmap;
    String url="http://203.92.41.131/raffle/webroot/img/raffle_provider_image/GuZCDgAY-raffle_logo.png";


    public Product_Image_Adapter(Context context, List<String> imageItm) {
        this.context = context;
       inflater=LayoutInflater.from(context);
       this.imageItm = imageItm;
        imageLoader= MySingleton.getInstance(context).getImageLoader();
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
//        View imageLayout = LayoutInflater.from(context).inflate(R.layout.product_item_image,view,false);



//        final ImageView imageView = (ImageView) imageLayout
//                .findViewById(R.id.img_product);
//        imageView.setImageResource(R.drawable.prodimg);
//
//       // Picasso.with(context).load(url).placeholder(R.drawable.prodimg).into(imageView);      //final TextView textView=(TextView)imageLayout.findViewById(R.id.splash_text1);
//       //   textView.setText(imageItm.get(position).getTitle());
//
//
//       // imageView.setImageResource(imageItm.get(position).);
//
//        imageLoader.get(imageItm.get(position), new ImageLoader.ImageListener() {
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
//                if(response.getBitmap()!=null){
// int height=response.getBitmap().getHeight();
//                    int width=response.getBitmap().getWidth();
//                    if (width >= 599 && height >= 700) {
//                        default_bitmap = Bitmap.createScaledBitmap(response.getBitmap(), 400, 400, false);
//                    } else {
//                        default_bitmap = Bitmap.createBitmap(response.getBitmap());
//                    }
//                    imageView
//                            .setImageBitmap(default_bitmap);
//
//
//                    //imageView.setImageBitmap(response.getBitmap());
//                }
//                else{
//                    imageView.setImageResource(R.drawable.prodimg);
//                }
//            }
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                imageView.setImageResource(R.drawable.prodimg);
//            }
//        });
//*/
//        view.addView(imageLayout);
        final ImageView imageView = new ImageView(view.getContext());

        ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
       // imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(imageParams);
        imageView.setPadding(5,5,5,5);

        LinearLayout layout = new LinearLayout(view.getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(layoutParams);
        layout.addView(imageView);
        imageLoader.get(imageItm.get(position), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                   /* int height=response.getBitmap().getHeight();
                    int width=response.getBitmap().getWidth();
                    if (width >= 599 && height >= 700) {
                        default_bitmap = Bitmap.createScaledBitmap(response.getBitmap(), 400, 400, false);
                    } else {
                        default_bitmap = Bitmap.createBitmap(response.getBitmap());
                    }
                    imageView
                            .setImageBitmap(default_bitmap);*/


                    imageView.setImageBitmap(response.getBitmap());
                }
                else{
                   // imageView.setImageResource(R.drawable.product_image_back);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
              //  imageView.setImageResource(R.drawable.product_image_back);
            }
        });
        view.addView(layout);

//*/
        return layout;
    }





    @Override
    public int getCount() {
        return imageItm.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
