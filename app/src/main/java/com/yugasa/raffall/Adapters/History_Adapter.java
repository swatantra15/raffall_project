package com.yugasa.raffall.Adapters;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.yugasa.raffall.HistoryDetails;
import com.yugasa.raffall.MySingleton;
import com.yugasa.raffall.R;

import java.util.List;

/**
 * Created by yugasalabs-45 on 27/9/17.
 */

public class History_Adapter extends RecyclerView.Adapter<History_Adapter.MyViewHolder> {
    List<HistoryDetails> historyDetailsList;
    ImageLoader imageLoader;
    Context context;

    public History_Adapter(List<HistoryDetails> historyDetailsList, Context context) {
        this.historyDetailsList = historyDetailsList;
        this.context = context;
        imageLoader= MySingleton.getInstance(context).getImageLoader();

    }



    @Override
    public History_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.userName.setText(historyDetailsList.get(position).userName);
        holder.address.setText(historyDetailsList.get(position).userAddress);
        holder.ticketPrice.setText("£"+historyDetailsList.get(position).productTokenPrice);
        holder.soldTicket.setText(historyDetailsList.get(position).soldToken);

        if (historyDetailsList.get(position).userReview.equals("null") && historyDetailsList.get(position).userReview==null ) {
        holder.review.setText(historyDetailsList.get(position).userReview);
        }
        else{
            holder.review.setText("Awaiting Review");
        }
        holder.productname.setText(historyDetailsList.get(position).productName);
        holder.wonDate.setText(historyDetailsList.get(position).endDate);

        if (!historyDetailsList.get(position).userRating.equals("null") && historyDetailsList.get(position).userRating!=null ) {
            holder.rating.setRating(Float.parseFloat(historyDetailsList.get(position).userRating));
        }
        else {
            holder.rating.setRating((float) 5);
        }

        imageLoader.get(historyDetailsList.get(position).raffalProductImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.productImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        imageLoader.get(historyDetailsList.get(position).userImage, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    holder.userImage.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });



    }


    @Override
    public int getItemCount() {
        return historyDetailsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView wonDate,soldTicket,ticketPrice,productname,review,userName,address;
        ImageView productImage,userImage;
        RatingBar rating;
        public MyViewHolder(View itemView) {
            super(itemView);
            wonDate=(TextView)itemView.findViewById(R.id.won_date);
            soldTicket=(TextView)itemView.findViewById(R.id.soldticket);
            ticketPrice=(TextView)itemView.findViewById(R.id.ticketPrice);
            productname=(TextView)itemView.findViewById(R.id.productName);
            rating=(RatingBar)itemView.findViewById(R.id.ratingBar);
            review=(TextView)itemView.findViewById(R.id.user_review);
            userName=(TextView)itemView.findViewById(R.id.username_txt);
            address=(TextView)itemView.findViewById(R.id.address);
            productImage=(ImageView) itemView.findViewById(R.id.productImage);
            userImage=(ImageView) itemView.findViewById(R.id.userImage);
        }
    }
}
