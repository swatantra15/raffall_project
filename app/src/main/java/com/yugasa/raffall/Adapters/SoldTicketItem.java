package com.yugasa.raffall.Adapters;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yugasalabs-45 on 25/9/17.
 */

public class SoldTicketItem {
    public String buyerName;
    public String country;
    public String totalbuyTicket;
    public String imageView1;
public SoldTicketItem(){}

    public SoldTicketItem(String buyerName, String country, String totalbuyTicket, String imageView1) {
        this.buyerName = buyerName;
        this.country = country;
        this.totalbuyTicket = totalbuyTicket;
        this.imageView1 = imageView1;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTotalbuyTicket(String totalbuyTicket) {
        this.totalbuyTicket = totalbuyTicket;
    }

    public void setImageView1(String imageView1) {
        this.imageView1 = imageView1;
    }


    public String getBuyerName() {
        return buyerName;
    }

    public String getCountry() {
        return country;
    }

    public String getTotalbuyTicket() {
        return totalbuyTicket;
    }

    public String getImageView1() {
        return imageView1;
    }






}