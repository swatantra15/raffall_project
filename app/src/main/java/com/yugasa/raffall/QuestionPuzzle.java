package com.yugasa.raffall;

import java.util.List;

/**
 * Created by yugasalabs-45 on 23/10/17.
 */

public class QuestionPuzzle {
    public String productName;
    public String totalToken;
    public String tokenPrice;
    public String availableToken;
    public String optionFormat;
    public List<PuzzleOption> rafflPuzzleOption;
    public RafflPuzzleQuestion rafflPuzzleQuestion;
    public PuzzleAnswer puzzleAnswer;

}
