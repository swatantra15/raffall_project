package com.yugasa.raffall;

/**
 * Created by yugasalabs-45 on 3/10/17.
 */

public class MyRaffle {
    public String id;
    public String userId;
    public String raffleProductId;
    public String no_of_token;
    public String productName;
    public String tokenPrice;
    public String totalToken;
    public String endCondition;
    public String productImage;
    public String soldToken;
    public String freeToken;
    public String runningStatus;

}
